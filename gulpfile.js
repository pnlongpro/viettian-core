// =============================
// 1. Project configuration
// =============================

var project = 'hemelios', // Project name, used for build zip.
	url = 'localhost:81/wpdemo/hemelios-core', // Local Development URL for BrowserSync. Change as-needed.
// Browser Sync port
	bs_port = 81,

	wp_content = './wp-content',
	theme_dir = './wp-content/themes/' + project,
	config_file = theme_dir + '/assets/css/less/config.less',


// Files that you want to package into a zip go here
	build = './build/',

// Main css fle to compile
	less_dir = theme_dir + '/assets/css/less',
	less_main_file = [less_dir + '/style.less',less_dir + '/rtl.less'],
	scss_dir = theme_dir + '/assets/scss',
	scss_main_file = scss_dir + '/style.scss',

// Uses it if your themes using a theme framework plugins such as darna-framework...
	framework_dir = theme_dir + '/theme-plugins',
	theme_plugin_name = 'hemelios-framework'
;

// =============================
// 2. Load all plugins
// =============================

var gulp = require('gulp'),
	browserSync = require('browser-sync'), // Asynchronous browser loading on .scss file changes
	reload = browserSync.reload,
	autoprefixer = require('gulp-autoprefixer'), // Autoprefixing magic
	//minifycss = require('gulp-uglifycss'),
	filter = require('gulp-filter'),
	//uglify = require('gulp-uglify'),
	//imagemin = require('gulp-imagemin'),
	//newer = require('gulp-newer'),
	//rename = require('gulp-rename'),
	concat = require('gulp-concat'),
	notify = require('gulp-notify'),
	//cmq = require('gulp-combine-media-queries'),
	//runSequence = require('gulp-run-sequence'),
	//sass = require('gulp-sass'),
	less = require('gulp-less'),
	//plugins = require('gulp-load-plugins')({camelize: true}),
	//ignore = require('gulp-ignore'), // Helps with ignoring files and directories in our run tasks
	//rimraf = require('gulp-rimraf'), // Helps with removing files and directories in our run tasks
	//zip = require('gulp-zip'), // Using to zip up our packaged theme into a tasty zip file that can be installed in WordPress!
	plumber = require('gulp-plumber'), // Helps prevent stream crashing on errors
	//cache = require('gulp-cache'),
	sourcemaps = require('gulp-sourcemaps'),
	header = require('gulp-header'),
	fs = require('fs');


// =============================
// 3. Single Tasks
// =============================

///**
// * 3.1. SCSS Compilation
// */
//// SCSS
//gulp.task('scss', function () {
//	gulp.src(scss_main_file)
//		.pipe(plumber())
//		.pipe(sourcemaps.init())
//		.pipe(sass({
//			errLogToConsole: true,
//			//outputStyle: 'compressed',
//			outputStyle    : 'compact',
//			// outputStyle: 'nested',
//			// outputStyle: 'expanded',
//			precision      : 10
//		}))
//		.pipe(sourcemaps.write({includeContent: false}))
//		.pipe(sourcemaps.init({loadMaps: true}))
//		.pipe(autoprefixer('last 2 version', '> 1%', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
//		.pipe(sourcemaps.write('.'))
//		.pipe(plumber.stop())
//		.pipe(gulp.dest(theme_dir))
//		.pipe(filter('**/*.css')) // Filtering stream to only css files
//		//.pipe(cmq()) // Combines Media Queries
//		.pipe(reload({stream: true})) // Inject Styles when style file is created
//		.pipe(rename({suffix: '.min'}))
//		.pipe(minifycss({
//			maxLineLen: 80
//		}))
//		.pipe(gulp.dest(theme_dir))
//		.pipe(reload({stream: true})) // Inject Styles when min style file is created
//		.pipe(notify({message: 'SCSS task complete', onLast: true}))
//});

// SCSS min
//gulp.task('scss-min', function () {
//	gulp.src(scss_main_file)
//		.pipe(plumber())
//		.pipe(sourcemaps.init())
//		.pipe(sass({
//			errLogToConsole: true,
//			//outputStyle: 'compressed',
//			outputStyle    : 'compact',
//			// outputStyle: 'nested',
//			// outputStyle: 'expanded',
//			precision      : 10
//		}))
//		.pipe(minifycss({
//			maxLineLen: 80
//		}))
//		.pipe(rename({suffix: '.min'}))
//		.pipe(autoprefixer('last 2 version', '> 1%', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
//		.pipe(sourcemaps.write(theme_dir))
//		.pipe(plumber.stop())
//		.pipe(gulp.dest(theme_dir))
//		.pipe(reload({stream: true})) // Inject Styles when min style file is created
//		.pipe(notify({message: 'Less min task complete', onLast: true}))
//});


/**
 * 3.2. Less Compilation
 */
// Less
gulp.task('less', function () {
	gulp.src(less_main_file)
		.pipe(plumber())
        .pipe(header(fs.readFileSync(config_file)))
        .pipe(header(fs.readFileSync(theme_dir + '/theme-info.txt', 'utf8')))
        .pipe(sourcemaps.init())
		.pipe(less())
		.pipe(sourcemaps.write('./'))
        .pipe(plumber.stop())
		.pipe(gulp.dest(theme_dir))
		.pipe(filter('**/*.css')) // Filtering stream to only css files
		//.pipe(cmq()) // Combines Media Queries
		.pipe(reload({stream: true})) // Inject Styles when style file is created
		.pipe(notify({message: 'Ngon Lành Cành Đào', onLast: true}))
});

gulp.task('autoprefixer' , function(){
    gulp.src(theme_dir + '/assets/css/less/**/*.less')
        .pipe(autoprefixer('last 2 version', '> 1%', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(gulp.dest('./'))
});

// Less min
//gulp.task('less-min', function () {
//	gulp.src(less_main_file)
//		.pipe(plumber())
//		.pipe(sourcemaps.init())
//		.pipe(less())
//		.pipe(minifycss({
//			maxLineLen: 80
//		}))
//		.pipe(rename({suffix: '.min'}))
//		.pipe(autoprefixer('last 2 version', '> 1%', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
//		.pipe(sourcemaps.write(theme_dir))
//		.pipe(plumber.stop())
//		.pipe(header(fs.readFileSync(theme_dir + '/theme-info.txt', 'utf8')))
//		.pipe(gulp.dest(theme_dir))
//		.pipe(reload({stream: true})) // Inject Styles when min style file is created
//		.pipe(notify({message: 'Less min task complete', onLast: true}))
//});


/**
 * 3.3. Cleaning task
 */

//// Clear cache
//gulp.task('clear', function () {
//	cache.clearAll();
//});
//
////Clean tasks for zipping build files
//gulp.task('cleanUp', function () {
//	return gulp.src([
//			theme_dir + '/**/.sass-cache',
//			theme_dir + '/**/.DS_Store'
//		], {read: false}) // much faster
//		.pipe(rimraf({force: true}))
//		.pipe(notify({message: 'Clean Up task complete', onLast: true}));
//});


/**
 *  3.4. Zipping themes directory for distribution
 */

//// Build Theme
//gulp.task('buildTheme', ['clear', 'cleanUp'], function () {
//	return gulp.src([
//			!wp_content + '/themes/*',
//			theme_dir + '/**'
//		], {base: wp_content + '/themes/'})
//		.pipe(zip(project + '.zip'))
//		.pipe(gulp.dest(build))
//		.pipe(notify({message: 'Zipping Project task complete', onLast: true}));
//});
//
//// Zipping Framework Plugin use for theme
//gulp.task('buildFP', ['clear', 'cleanUp'], function () {
//	return gulp.src([
//			!wp_content + '/plugins/*',
//			wp_content + '/plugins/' + theme_plugin_name + '/**'
//		], {base: wp_content + '/plugins/'})
//		.pipe(zip(theme_plugin_name + '.zip'))
//		.pipe(gulp.dest(framework_dir))
//		.pipe(notify({message: 'Zipping Theme Plugin task complete', onLast: true}));
//
//});


// Zipping Theme plugins and theme for distribution
// Just use this task if your theme use a theme plugin (such as darna-framework...)
//gulp.task('buildThemeAndPlugin', function () {
//	runSequence('clear', 'cleanUp', 'buildFP', 'buildTheme');
//});

/**
 * 3.5. Browser Sync
 */
gulp.task('browser-sync', function () {
	//Watching these files
	var files = [
		theme_dir + '/**/*.php',
		theme_dir + '/assets/js/**/*.js'
	];
	browserSync.init(files, {
		proxy        : url,
		port         : bs_port,
		injectChanges: true, // inject css
		notify       : true
	});
});



// =============================
// 4. Serving and Default tasks
// =============================

/**
 * Serve Less Task
 */
gulp.task('serve-less', ['less', 'browser-sync'], function () {
	gulp.watch(less_dir + '/**/*.less', ['less']);
});


/**
 * Serve SCSS Task
 */
gulp.task('serve-scss', ['less', 'browser-sync'], function () {
	gulp.watch(scss_dir + '/**/*.scss', ['scss']);
	gulp.watch(theme_dir + '/assets/js/**/*.js', [browserSync.reload]);
});


/**
 * Gulp Default Task
 * Run the serve task using css preprocessor that your theme use.
 */
gulp.task('default', ['serve-less']);


<?php

/**
 *
 */

if ( !class_exists( 'OS_Opt' ) ) {

	final class OS_Opt {

		private static $instance = null;

		private $args = array();
		private $sections = array();
		private $ReduxFramework;

		private function __construct() {
			if ( !class_exists( 'ReduxFramework' ) ) {
				return;
			}
			// This is needed. Bah WordPress bugs.  ;)
			/*if ( true == Redux_Helpers::isTheme( __FILE__ ) ) {
				$this->initSettings();
			} else {
				add_action( 'plugins_loaded', array( $this, 'initSettings' ), 10 );
			}*/

			$this->setArguments();
			$this->setHelpTabs();

			if ( !isset( $this->args['opt_name'] ) ) { // No errors please
				return;
			}
		}

		/**
		 * Get OS_Opt Object
		 * @return null|OS_Opt
		 */
		public static function getInstance() {
			if ( self::$instance == null ) {
				self::$instance = new OS_Opt();
			}

			return self::$instance;
		}

		/**
		 * Set Redux FW Object
		 * @param $reduxOj
		 */
		public function setReduxFW( $reduxOj ) {
			$this->ReduxFramework = $reduxOj;
		}


		/**
		 * Add Section to Sections
		 */
		public function addSection( array $section ) {
			$this->sections[] = $section;
		}

		/**
		 * Get a sections
		 * @return array
		 */
		public function getSection() {
			return $this->sections;
		}

		public function getArgs() {
			return $this->args;
		}

		public function setHelpTabs() {
		}

		/**
		 * All the possible arguments for Redux.
		 * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
		 * */
		public function setArguments() {

			$theme = wp_get_theme(); // For use with some settings. Not necessary.

			$this->args = array(
				// TYPICAL -> Change these values as you need/desire
				'opt_name'        => 'hemelios_hemelios_options',
				// This is where your data is stored in the database and also becomes your global variable name.
				'display_name'    => $theme->get( 'Name' ),
				// Name that appears at the top of your panel
				'display_version' => $theme->get( 'Version' ),
				// Version that appears at the top of your panel
				'menu_type'       => 'menu',
				//Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
				'allow_sub_menu'  => true,
				// Show the sections below the admin menu item or not
				'menu_title'      => esc_html__( 'Theme Options', 'hemelios' ),
				'page_title'      => esc_html__( 'Theme Options', 'hemelios' ),
				// You will need to generate a Google API key to use this feature.
				// Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
				'google_api_key'  => '',
				// Must be defined to add google fonts to the typography module

				'async_typography'   => false,
				// Use a asynchronous font on the front end or font string
				'admin_bar'          => true,
				// Show the panel pages on the admin bar
				'global_variable'    => '',
				// Set a different name for your global variable other than the opt_name
				'dev_mode'           => false,
				// Show the time the page took to load, etc
				'customizer'         => true,
				// Enable basic customizer support

				// OPTIONAL -> Give you extra features
				'page_priority'      => null,
				// Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
				'page_parent'        => 'themes.php',
				// For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_theme_page#Parameters
				'page_permissions'   => 'manage_options',
				// Permissions needed to access the options panel.
				'menu_icon'          => '',
				// Specify a custom URL to an icon
				'last_tab'           => '',
				// Force your panel to always open to a specific tab (by id)
				'page_icon'          => 'icon-themes',
				// Icon displayed in the admin panel next to your menu_title
				'page_slug'          => '_options',
				// Page slug used to denote the panel
				'save_defaults'      => true,
				// On load save the defaults to DB before user clicks save or not
				'default_show'       => false,
				// If true, shows the default value next to each field that is not the default value.
				'default_mark'       => '',
				// What to print by the field's title if the value shown is default. Suggested: *
				'show_import_export' => true,
				// Shows the Import/Export panel when not used as a field.

				// CAREFUL -> These options are for advanced use only
				'transient_time'     => 60 * MINUTE_IN_SECONDS,
				'output'             => true,
				// Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
				'output_tag'         => true,
				// Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
				// 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

				// FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
				'database'           => '',
				// possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
				'system_info'        => false,
				// REMOVE

				// HINTS
				'hints'              => array(
					'icon'          => 'icon-question-sign',
					'icon_position' => 'right',
					'icon_color'    => 'lightgray',
					'icon_size'     => 'normal',
					'tip_style'     => array(
						'color'   => 'light',
						'shadow'  => true,
						'rounded' => false,
						'style'   => '',
					),
					'tip_position'  => array(
						'my' => 'top left',
						'at' => 'bottom right',
					),
					'tip_effect'    => array(
						'show' => array(
							'effect'   => 'slide',
							'duration' => '500',
							'event'    => 'mouseover',
						),
						'hide' => array(
							'effect'   => 'slide',
							'duration' => '500',
							'event'    => 'click mouseleave',
						),
					),
				)
			);


			// SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
			$this->args['share_icons'][] = array(
				'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
				'title' => 'Visit us on GitHub',
				'icon'  => 'el el-github'
				//'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
			);
			$this->args['share_icons'][] = array(
				'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
				'title' => 'Like us on Facebook',
				'icon'  => 'el el-facebook'
			);
			$this->args['share_icons'][] = array(
				'url'   => 'http://twitter.com/reduxframework',
				'title' => 'Follow us on Twitter',
				'icon'  => 'el el-twitter'
			);
			$this->args['share_icons'][] = array(
				'url'   => 'http://www.linkedin.com/company/redux-framework',
				'title' => 'Find us on LinkedIn',
				'icon'  => 'el el-linkedin'
			);

			// Panel Intro text -> before the form
			if ( !isset( $this->args['global_variable'] ) || $this->args['global_variable'] !== false ) {
				if ( !empty( $this->args['global_variable'] ) ) {
					$v = $this->args['global_variable'];
				} else {
					$v = str_replace( '-', '_', $this->args['opt_name'] );
				}
				//$this->args['intro_text'] = sprintf( esc_html__( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'hemelios' ), $v );
			} else {
				//$this->args['intro_text'] = esc_html__( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'hemelios' );
			}

			// Add content after the form.
			$allowed_html = array(
				'i'  => array(
					'class' => array(),
				),
				'p'  => array(),
				'em' => array(),
			);
//			$this->args['footer_text'] = wp_kses( __( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'hemelios' ), $allowed_html );
		}

	}

	global $osOpt;
	$osOpt = OS_Opt::getInstance();


	// General Setting
	locate_template('/includes/options-setup/section-general-setting.php', true, true );
	// --Maintenance Mode
	locate_template('/includes/options-setup/section-general-setting-maintainance.php', true, true );
	// --Performance Options
	locate_template('/includes/options-setup/section-general-setting-performance.php', true, true );
	// --Custom Favicon
	locate_template('/includes/options-setup/section-general-setting-favicon.php', true, true );
	// --404
	locate_template('/includes/options-setup/section-general-setting-404.php', true, true );
	// Styling Options
	locate_template('/includes/options-setup/section-color.php', true, true );
	// Font Options
	locate_template('/includes/options-setup/section-fonts.php', true, true );
	// Logo
	locate_template('/includes/options-setup/section-logo.php', true, true );
	// Header
	locate_template('/includes/options-setup/section-header.php', true, true );
	// --Top drawer Setting
//	locate_template('/includes/options-setup/section-header-top-drawer.php', true, true );
	// --Top bar setting
	locate_template('/includes/options-setup/section-header-top-bar.php', true, true );
	// --Header Menu
	locate_template('/includes/options-setup/section-header-menu.php', true, true );
	// Mobile Header
	locate_template('/includes/options-setup/section-header-mobile.php', true, true );
	// Footer
	locate_template('/includes/options-setup/section-footer.php', true, true );
	// Layout Setting
	locate_template('/includes/options-setup/section-layout.php', true, true );
	// --Page Layout
	locate_template('/includes/options-setup/section-layout-page.php', true, true );
	// --Archive Layout
	locate_template('/includes/options-setup/section-layout-archive.php', true, true );
	// --Single Post Layout
	locate_template('/includes/options-setup/section-layout-single-post.php', true, true );
	// --Single Product Layout
	locate_template('/includes/options-setup/section-layout-single-product.php', true, true );
	// --Archive Product Layout
	locate_template('/includes/options-setup/section-layout-product-archive.php', true, true );
	//Shop
	locate_template('/includes/options-setup/section-shop.php', true, true );
	// Custom Post Type
	locate_template('/includes/options-setup/section-custom-post-type.php', true, true );
	// --Portfolio Options
	locate_template('/includes/options-setup/section-custom-post-type-portfolio.php', true, true );
	// --Services Options
	locate_template('/includes/options-setup/section-custom-post-type-services.php', true, true );
	// --Ourteam Options
	locate_template('/includes/options-setup/section-custom-post-type-ourteam.php', true, true );
	//Social Profiles
	locate_template('/includes/options-setup/section-social-profile.php', true, true );
	// Resources CDN
	locate_template('/includes/options-setup/section-resources-cdn.php', true, true );
	// Custom css - js
	locate_template('/includes/options-setup/section-custom-css-js.php', true, true );


	$osOpt->setReduxFW( new ReduxFramework( $osOpt->getSection(), $osOpt->getArgs() ) );
}


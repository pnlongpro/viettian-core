<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

global $osOpt;

$osOpt->addSection(
	array(
		'title'      => esc_html__( 'Top Bar', 'hemelios' ),
		'desc'       => '',
		'icon'       => 'el el-briefcase',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => 'top_bar',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Show/Hide Top Bar', 'hemelios' ),
				'subtitle' => esc_html__( 'Show or hide top bar.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '0',
			),

			array(
				'id'       => 'top_bar_layout',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Top bar Layout', 'hemelios' ),
				'subtitle' => esc_html__( 'Select the top bar column layout.', 'hemelios' ),
				'desc'     => '',
				'options'  => array(
					'top-bar-1' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/top-bar-layout-1.jpg' ),
					'top-bar-2' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/top-bar-layout-2.jpg' ),
					'top-bar-3' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/top-bar-layout-3.jpg' ),
					'top-bar-4' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/top-bar-layout-4.jpg' ),
				),
				'default'  => 'top-bar-1',
				'required' => array( 'top_bar', '=', '1' ),
			),

			array(
				'id'       => 'top_bar_left_sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Top Left Sidebar', 'hemelios' ),
				'subtitle' => "Choose the top left sidebar.",
				'data'     => 'sidebars',
				'desc'     => '',
				'default'  => 'top_bar_left',
				'required' => array( 'top_bar', '=', '1' ),
			),
			array(
				'id'       => 'top_bar_right_sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Top Right Sidebar', 'hemelios' ),
				'subtitle' => "Choose the top right sidebar.",
				'data'     => 'sidebars',
				'desc'     => '',
				'default'  => 'top_bar_right',
				'required' => array( 'top_bar', '=', '1' )
			),
			array(
				'id'       => 'show_top_bar',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Show/Hide Top Bar Border', 'hemelios' ),
				'subtitle' => esc_html__( 'Show or hide top bar border.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'Show', '0' => 'Hide' ),
				'default'  => '1',
				'required' => array( 'top_bar', '=', '1' )
			),

			array(
				'id'       => 'topdbar_divide',
				'type'     => 'divide',
			),

			array(
				'id'       => 'top_bar_bg_color',
				'type'     => 'color_rgba',
				'title'    => esc_html__( 'Top Bar Background Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set top bar background color.', 'hemelios' ),
				'default'  => array(
					'color' => '#F8F8F8',
					'alpha' => '1'
				),
				'mode'     => 'background',
				'validate' => 'colorrgba',
			),

			array(
				'id'       => 'top_bar_text_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Top Bar Text Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Pick a text color for the top bar', 'hemelios' ),
				'default'  => '#888888',
				'validate' => 'color',
			),
			array(
				'id'       => 'top_bar_border_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Top Bar Border Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Pick a text border color for the top bar.', 'hemelios' ),
				'default'  => '#eee',
				'validate' => 'color',
			)
		)
	) );
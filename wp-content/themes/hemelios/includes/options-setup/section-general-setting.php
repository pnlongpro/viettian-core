<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:21 AM
 */

global $osOpt;

$osOpt->addSection(array(
	'title'  => esc_html__( 'General Setting', 'hemelios' ),
	'desc'   => '',
	'icon'   => 'el el-wrench',
	'fields' => array(
		array(
			'id'       => 'home_preloader',
			'type'     => 'select',
			'title'    => esc_html__( 'Preloader', 'hemelios' ),
			'subtitle' => esc_html__( 'Select loading style when your page is on load.', 'hemelios' ),
			'desc'     => '',
			'options'  => array(
				'none'       => 'None',
				'square-1'   => 'Square 01',
				'square-2'   => 'Square 02',
				'square-3'   => 'Square 03',
				'square-4'   => 'Square 04',
				'square-5'   => 'Square 05',
				'square-6'   => 'Square 06',
				'square-7'   => 'Square 07',
				'square-8'   => 'Square 08',
				'square-9'   => 'Square 09',
				'round-1'    => 'Round 01',
				'round-2'    => 'Round 02',
				'round-3'    => 'Round 03',
				'round-4'    => 'Round 04',
				'round-5'    => 'Round 05',
				'round-6'    => 'Round 06',
				'round-7'    => 'Round 07',
				'round-8'    => 'Round 08',
				'round-9'    => 'Round 09',
				'various-1'  => 'Various 01',
				'various-2'  => 'Various 02',
				'various-3'  => 'Various 03',
				'various-4'  => 'Various 04',
				'various-5'  => 'Various 05',
				'various-6'  => 'Various 06',
				'various-7'  => 'Various 07',
				'various-8'  => 'Various 08',
				'various-9'  => 'Various 09',
				'various-10' => 'Various 10',

			),
			'default'  => 'none'
		),

		array(
			'id'       => 'home_preloader_bg_color',
			'type'     => 'color_rgba',
			'title'    => esc_html__( 'Preloader background color', 'hemelios' ),
			'subtitle' => esc_html__( 'Set preloader background color.', 'hemelios' ),
			'default'  => array(),
			'mode'     => 'background',
			'validate' => 'colorrgba',
			'required' => array( 'home_preloader', 'not_empty_and', array( 'none' ) ),
		),

		array(
			'id'       => 'home_preloader_spinner_color',
			'type'     => 'color',
			'title'    => esc_html__( 'Preloader spinner color', 'hemelios' ),
			'subtitle' => esc_html__( 'Pick a preloader spinner color for the top bar.', 'hemelios' ),
			'default'  => '#0cb4ce',
			'validate' => 'color',
			'required' => array( 'home_preloader', 'not_empty_and', array( 'none' ) ),
		),

		array(
			'id'       => 'smooth_scroll',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Smooth Scroll', 'hemelios' ),
			'subtitle' => esc_html__( 'Enable or disable smooth scroll.', 'hemelios' ),
			'desc'     => '',
			'options'  => array( '1' => 'On', '0' => 'Off' ),
			'default'  => '0'
		),

		array(
			'id'       => 'custom_scroll',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Custom Scroll Bar', 'hemelios' ),
			'subtitle' => esc_html__( 'Enable or disable custom scroll bar.', 'hemelios' ),
			'desc'     => '',
			'options'  => array( '1' => 'On', '0' => 'Off' ),
			'default'  => '0'
		),

		array(
			'id'       => 'custom_scroll_width',
			'type'     => 'text',
			'title'    => esc_html__( 'Custom Scroll Bar Width', 'hemelios' ),
			'subtitle' => esc_html__( 'This must be numeric (no px) or empty.', 'hemelios' ),
			'validate' => 'numeric',
			'default'  => '10',
			'required' => array( 'custom_scroll', '=', array( '1' ) ),
		),

		array(
			'id'       => 'custom_scroll_color',
			'type'     => 'color',
			'title'    => esc_html__( 'Custom Scroll Bar Color', 'hemelios' ),
			'subtitle' => esc_html__( 'Set custom scroll bar color.', 'hemelios' ),
			'default'  => '#eeeeee',
			'validate' => 'color',
			'required' => array( 'custom_scroll', '=', array( '1' ) ),
		),

		array(
			'id'       => 'custom_scroll_thumb_color',
			'type'     => 'color',
			'title'    => esc_html__( 'Custom Scroll Bar Thumb Color', 'hemelios' ),
			'subtitle' => esc_html__( 'Set custom scroll bar thumb color.', 'hemelios' ),
			'default'  => '#0cb4ce',
			'validate' => 'color',
			'required' => array( 'custom_scroll', '=', array( '1' ) ),
		),


		array(
			'id'       => 'panel_selector',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Panel Selector', 'hemelios' ),
			'subtitle' => esc_html__( 'Enable or disable panel selector.', 'hemelios' ),
			'desc'     => '',
			'options'  => array( '1' => 'On', '0' => 'Off' ),
			'default'  => '0'
		),
		array(
			'id'       => 'back_to_top',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Back To Top', 'hemelios' ),
			'subtitle' => esc_html__( 'Enable or disable back to top button.', 'hemelios' ),
			'desc'     => '',
			'options'  => array( '1' => 'On', '0' => 'Off' ),
			'default'  => '1'
		),

		array(
			'id'       => 'enable_rtl_mode',
			'type'     => 'button_set',
			'title'    => esc_html__( 'RTL mode', 'hemelios' ),
			'subtitle' => esc_html__( 'Enable or disable rtl mode.', 'hemelios' ),
			'desc'     => '',
			'options'  => array( '1' => 'On', '0' => 'Off' ),
			'default'  => '0'
		),


		array(
			'id'       => 'enable_social_meta',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Social Meta Tags', 'hemelios' ),
			'subtitle' => esc_html__( 'Enable or disable the social meta head tag output.', 'hemelios' ),
			'desc'     => '',
			'options'  => array( '1' => 'On', '0' => 'Off' ),
			'default'  => '0'
		),

		array(
			'id'       => 'twitter_author_username',
			'type'     => 'text',
			'title'    => esc_html__( 'Twitter Publisher Username.', 'hemelios' ),
			'subtitle' => esc_html__( 'Enter your twitter username here, to be used for the twitter card date. ensure that you do not include the @ symbol.', 'hemelios' ),
			'desc'     => '',
			'default'  => "",
			'required' => array( 'enable_social_meta', '=', array( '1' ) ),
		),
		array(
			'id'       => 'googleplus_author',
			'type'     => 'text',
			'title'    => esc_html__( 'Google+ Username', 'hemelios' ),
			'subtitle' => esc_html__( 'Enter your google+ username here, to be used for the authorship meta.', 'hemelios' ),
			'desc'     => '',
			'default'  => "",
			'required' => array( 'enable_social_meta', '=', array( '1' ) ),
		),


		array(
			'id'   => 'general_divide_2',
			'type' => 'divide'
		),
		array(
			'id'       => 'layout_style',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Layout Style', 'hemelios' ),
			'subtitle' => esc_html__( 'Select the layout style.', 'hemelios' ),
			'desc'     => '',
			'options'  => array(
				'boxed' => array( 'title' => 'Boxed', 'img' => get_template_directory_uri() . '/assets/images/theme-options/layout-boxed.png' ),
				'wide'  => array( 'title' => 'Wide', 'img' => get_template_directory_uri() . '/assets/images/theme-options/layout-wide.png' )
			),
			'default'  => 'wide'
		),


		array(
			'id'       => 'body_background_mode',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Body Background Style', 'hemelios' ),
			'subtitle' => esc_html__( 'Choose background style.', 'hemelios' ),
			'desc'     => '',
			'options'  => array( 'background' => 'Background', 'pattern' => 'Pattern' ),
			'default'  => 'background',
			'required' => array( 'layout_style', '=', array( 'boxed' ) )
		),

		array(
			'id'       => 'body_background',
			'type'     => 'background',
			'output'   => array( 'body' ),
			'title'    => esc_html__( 'Body Background', 'hemelios' ),
			'subtitle' => esc_html__( 'Body background (only for boxed layout style).', 'hemelios' ),
			'default'  => array(
				'background-color'      => '',
				'background-repeat'     => 'no-repeat',
				'background-position'   => 'center center',
				'background-attachment' => 'fixed',
				'background-size'       => 'cover'
			),
			'required' => array(
				array( 'layout_style', '=', array( 'boxed' ) ),
				array( 'body_background_mode', '=', array( 'background' ) )
			),
		),
		array(
			'id'       => 'body_background_pattern',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Background Pattern', 'hemelios' ),
			'subtitle' => esc_html__( 'Body background pattern (only for boxed layout style)', 'hemelios' ),
			'desc'     => '',
			'height'   => '40px',
			'options'  => array(
				'pattern-1.png' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/pattern-1.png' ),
				'pattern-2.png' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/pattern-2.png' ),
				'pattern-3.png' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/pattern-3.png' ),
				'pattern-4.png' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/pattern-4.png' ),
				'pattern-5.png' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/pattern-5.png' ),
				'pattern-6.png' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/pattern-6.png' ),
				'pattern-7.png' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/pattern-7.png' ),
				'pattern-8.png' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/pattern-8.png' ),
			),
			'default'  => 'pattern-1.png',
			'required' => array(
				array( 'layout_style', '=', array( 'boxed' ) ),
				array( 'body_background_mode', '=', array( 'pattern' ) )
			),
		),
		array(
			'id'       => 'tracking_code',
			'type'     => 'ace_editor',
			'mode'     => 'plain_text',
			'theme'    => 'monokai',
			'title'    => esc_html__( 'Tracking Code', 'hemelios' ),
			'subtitle' => esc_html__( 'Add some tracking code to here (facebook, google analytics, Chat online...)', 'hemelios' ),
			'desc'     => '',
			'default'  => '',
			'options'  => array( 'minLines' => 20, 'maxLines' => 60 )
		),

	)
));
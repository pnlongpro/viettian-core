<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

$page_404_bg_url = get_template_directory_uri() . '/assets/images/bg-404.jpg';

global $osOpt;

$osOpt->addSection(
	array(
		'title'      => esc_html__( '404 Setting', 'hemelios' ),
		'desc'       => '',
		'subsection' => true,
		'icon'       => 'el el-error',
		'fields'     => array(
			array(
				'id'       => 'page_404_bg_image',
				'type'     => 'media',
				'url'      => true,
				'title'    => esc_html__( 'Background page', 'hemelios' ),
				'subtitle' => esc_html__( 'Upload your background image here.', 'hemelios' ),
				'desc'     => '',
				'default'  => array(
					'url' => $page_404_bg_url
				)
			),
			array(
				'id'      => 'subtitle_404',
				'type'    => 'textarea',
				'title'   => esc_html__( 'Subtitle 404', 'hemelios' ),
				'default' => 'The page you’re looking for not found',
			),
		)
	) );
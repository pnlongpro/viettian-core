<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

global $osOpt;

$osOpt->addSection(
	array(
		'title'      => esc_html__( 'Top Drawer', 'hemelios' ),
		'desc'       => '',
		'icon'       => 'el el-bookmark',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => 'top_drawer_type',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Top Drawer Mode', 'hemelios' ),
				'subtitle' => esc_html__( 'Select top drawer mode.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( 'none' => 'Disable', 'show' => 'Always Show', 'toggle' => 'Toggle' ),
				'default'  => 'none'
			),
			array(
				'id'       => 'top_drawer_sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Top Drawer Sidebar', 'hemelios' ),
				'subtitle' => "Choose the top drawer sidebar.",
				'data'     => 'sidebars',
				'desc'     => '',
				'default'  => 'top_drawer_sidebar',
				'required' => array( 'top_drawer_type', '=', array( 'show', 'toggle' ) ),
			),
			array(
				'id'       => 'top_drawer_layout',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Top Drawer Layout', 'hemelios' ),
				'subtitle' => esc_html__( 'Select the top drawer column layout.', 'hemelios' ),
				'desc'     => '',
				'options'  => array(
					'top-drawer-1' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-1.jpg' ),
					'top-drawer-2' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-2.jpg' ),
					'top-drawer-3' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-3.jpg' ),
					'top-drawer-4' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-4.jpg' ),
					'top-drawer-5' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-5.jpg' ),
					'top-drawer-6' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-6.jpg' ),
					'top-drawer-7' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-7.jpg' ),
					'top-drawer-8' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-8.jpg' ),
					'top-drawer-9' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-9.jpg' ),
				),
				'default'  => 'top-drawer-9',
				'required' => array( 'top_drawer_type', '=', array( 'show', 'toggle' ) ),
			),
			array(
				'id'       => 'top_drawer_wrapper_layout',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Top Drawer Wrapper Layout', 'hemelios' ),
				'subtitle' => esc_html__( 'Select top drawer wrapper layout.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( 'full' => 'Full Width', 'container' => 'Container', 'container-fluid' => 'Container Fluid' ),
				'default'  => 'container',
				'required' => array( 'top_drawer_type', '=', array( 'show', 'toggle' ) ),
			),

			array(
				'id'       => 'topdrawer_divide',
				'type'     => 'divide',
				'required' => array( 'top_drawer_type', '=', array( 'show', 'toggle' ) )
			),

			array(
				'id'       => 'top_drawer_bg_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Top Drawer Background Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set top drawer background color.', 'hemelios' ),
				'default'  => '#2f2f2f',
				'validate' => 'color',
				'required' => array( 'top_drawer_type', '=', array( 'show', 'toggle' ) )
			),

			array(
				'id'       => 'top_drawer_text_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Top Drawer Text Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Pick a text color for the top drawer.', 'hemelios' ),
				'default'  => '#c5c5c5',
				'validate' => 'color',
				'required' => array( 'top_drawer_type', '=', array( 'show', 'toggle' ) )
			)
		)
	) );
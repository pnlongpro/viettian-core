<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

global $osOpt;

$osOpt->addSection(
	array(
		'title'  => esc_html__( 'Header', 'hemelios' ),
		'desc'   => '',
		'icon'   => 'el el-credit-card',
		'fields' => array(
//					Header layout setting
			array(
				'id'     => 'section-header-layout',
				'type'   => 'section',
				'title'  => esc_html__( 'Header Layout', 'hemelios' ),
				'indent' => true
			),
			array(
				'id'       => 'header_layout_style',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Header Layout Style', 'hemelios' ),
				'subtitle' => esc_html__( 'Select the header layout style.', 'hemelios' ),
				'desc'     => '',
				'options'  => array(
					'boxed' => array( 'title' => 'Boxed', 'img' => get_template_directory_uri() . '/assets/images/theme-options/layout-boxed.png' ),
					'wide'  => array( 'title' => 'Wide', 'img' => get_template_directory_uri() . '/assets/images/theme-options/layout-wide.png' )
				),
				'default'  => 'wide'
			),
			array(
				'id'       => 'header_layout',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Header Layout', 'hemelios' ),
				'subtitle' => esc_html__( 'Select a header layout option from the examples. Remember to regenerate css if you decide to change header layout.', 'hemelios' ),
				'desc'     => '',
				'options'  => array(
					'header-1' => array( 'title' => 'Header 1', 'img' => get_template_directory_uri() . '/assets/images/theme-options/header-1.jpg' ),
					'header-2' => array( 'title' => 'Header 2', 'img' => get_template_directory_uri() . '/assets/images/theme-options/header-2.jpg' ),
					'header-3' => array( 'title' => 'Header 3', 'img' => get_template_directory_uri() . '/assets/images/theme-options/header-3.jpg' ),
				),
				'default'  => 'header-1'
			),
			array(
				'id'       => 'header_positon',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Header Position', 'hemelios' ),
				'subtitle' => esc_html__( 'Set header position.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '0' => 'Default', '1' => 'Overlay' ),
				'default'  => '0',
			),
			array(
				'id'      => 'header_margin_top',
				'type'    => 'dimensions',
				'title'   => esc_html__( 'Header Margin Top', 'hemelios' ),
				'desc'    => esc_html__( 'You can set a margin top for the header here.', 'hemelios' ),
				'units'   => 'px',
				'width'   => false,
				'default' => array(
					'height' => '0'
				)
			),
			array(
				'id'       => 'header_border',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Header Border', 'hemelios' ),
				'subtitle' => esc_html__( 'Show or hide header top and bottom border.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'Show', '0' => 'Hide' ),
				'default'  => '1',
			),
			//Header Color
			array(
				'id'     => 'section-header-layout',
				'type'   => 'section',
				'title'  => esc_html__( 'Header Color', 'hemelios' ),
				'indent' => true
			),
			array(
				'id'       => 'header_bg',
				'type'     => 'color_rgba',
				'title'    => esc_html__( 'Header Background Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set header background color.', 'hemelios' ),
				'default'  => array(
					'color' => '#fff',
					'alpha' => '1'
				),
				'mode'     => 'background',
				'validate' => 'colorrgba',
			),
			array(
				'id'       => 'header_border_color',
				'type'     => 'color_rgba',
				'title'    => esc_html__( 'Header Border Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set header border color.', 'hemelios' ),
				'default'  => array(
					'color' => '#eee',
					'alpha' => '0.3'
				),
				'validate' => 'colorrgba',
			),
			array(
				'id'       => 'header_sticky_bg',
				'type'     => 'color',
				'title'    => esc_html__( 'Header Sticky Background Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set header sticky background color.', 'hemelios' ),
				'default'  => '#fff',
				'validate' => 'color',
				'required' => array( 'header_sticky', '=', '1' )
			),

//					Menu setting
			array(
				'id'     => 'section-header-customize',
				'type'   => 'section',
				'title'  => esc_html__( 'Header Customize', 'hemelios' ),
				'indent' => true
			),
			array(
				'id'       => 'header_sticky',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Header Sticky', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable hide header sticky.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '1'
			),
			array(
				'id'       => 'header_sticky_border',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Header Sticky Border Bottom', 'hemelios' ),
				'subtitle' => esc_html__( 'Show or hide header sticky border bottom.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'Show', '0' => 'Hide' ),
				'default'  => '1',
				'required' => array( 'header_sticky', '=', '1' ),
			),
			array(
				'id'       => 'header_customize',
				'type'     => 'sorter',
				'title'    => 'Header Component Customize',
				'subtitle' => 'Choose which component will appear on your header.',
				'desc'     => 'Drag and drop the components on these boxes above.',
				'options'  => array(
					'enabled'  => array(
						'shopping-cart' => 'Shopping Cart',
					),
					'disabled' => array(
						'search'      => 'Search Box',
						'get-a-quote' => 'Get a quote',
					)
				)
			),

			array(
				'id'       => 'main_menu_after_customize',
				'type'     => 'ace_editor',
				'mode'     => 'html',
				'theme'    => 'monokai',
				'title'    => esc_html__( 'Custom Text For Header 3', 'hemelios' ),
				'subtitle' => esc_html__( 'Set custom text for header 3.', 'hemelios' ),
				'desc'     => '',
				'default'  => '<ul class="header-custom-text">
	<li>
		<i class="icon icon-clock"></i>
		<p>
			Mon - Sat: 9:00 - 18:00
			<span>Sunday CLOSED</span>
		</p>
	</li>
	<li>
		<i class="icon icon-map-pin"></i>
		<p>
			1422 1st St. Santa Rosa
			<span>CA 94559. United States</span>
		</p>
	</li>
	<li>
		<i class="icon icon-phone"></i>
		<p>
			(123) 45678910
			<span>info@osthemes.com</span>
		</p>
	</li>
</ul>

',
				'options'  => array( 'minLines' => 4, 'maxLines' => 60 ),
				'required' => array( 'header_layout', '=', 'header-3' )
			),
			array(
				'id'       => 'header_shopping_cart_button',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Shopping Cart Button', 'hemelios' ),
				'subtitle' => esc_html__( 'Select header shopping cart button.', 'hemelios' ),
				'options'  => array(
					'view-cart' => 'View Cart',
					'checkout'  => 'Checkout',
				),
				'default'  => array(
					'view-cart' => '1',
					'checkout'  => '1',
				),
				//'required' => array( 'header_shopping_cart', '=', '1' ),
			),

			array(
				'id'       => 'view_cart_subtotal',
				'type'     => 'button_set',
				'title'    => esc_html__( 'View Cart Sub Total', 'hemelios' ),
				'subtitle' => esc_html__( 'Show or hide total cash on heade.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'Show', '0' => 'Hide' ),
				'default'  => '0',
			),

			array(
				'id'       => 'custom_cart_quantity_icon_bg',
				'type'     => 'color',
				'title'    => esc_html__( 'Custom Cart Quantity Icon Background', 'hemelios' ),
				'subtitle' => esc_html__( 'Set custom cart quantity icon background color.', 'hemelios' ),
				'desc'     => 'Using default value (clear the value field) if you want to use the primary color.',
				'validate' => 'color',
				'default'  => '',
			),


			array(
				'id'       => 'search_box_type',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Search Box Type', 'hemelios' ),
				'subtitle' => esc_html__( 'Select search box type.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( 'standard' => esc_html__( 'Standard', 'hemelios' ), 'ajax' => esc_html__( 'Ajax Search', 'hemelios' ) ),
				'default'  => 'standard',
//						'required' => array('header_customize', 'enabled', '')
			),

			array(
				'id'       => 'search_box_post_type',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Post type for Ajax Search', 'hemelios' ),
				'subtitle' => esc_html__( 'Select post type for ajax search.', 'hemelios' ),
				'options'  => array(
					'post'      => 'Post',
					'page'      => 'Page',
					'product'   => 'Product',
					'portfolio' => 'Portfolio',
					'services'  => 'Our Services',
				),
				'default'  => array(
					'post'      => '1',
					'page'      => '1',
					'product'   => '1',
					'portfolio' => '1',
					'services'  => '1',
				),
				'required' => array( 'search_box_type', '=', 'ajax' ),
			),

			array(
				'id'       => 'search_box_result_amount',
				'type'     => 'text',
				'title'    => esc_html__( 'Amount Of Search Result', 'hemelios' ),
				'subtitle' => esc_html__( 'This must be numeric (no px) or empty (default: 8).', 'hemelios' ),
				'desc'     => esc_html__( 'Set mount of search result.', 'hemelios' ),
				'validate' => 'numeric',
				'default'  => '',
				'required' => array( 'search_box_type', '=', 'ajax' ),
			),
		)
	) );
<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:28 AM
 */

global $osOpt;

$osOpt->addSection(array(
	'title'      => esc_html__( 'Maintenance Mode', 'hemelios' ),
	'desc'       => '',
	'subsection' => true,
	'icon'       => 'el-icon-eye-close',
	'fields'     => array(
		array(
			'id'       => 'enable_maintenance',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Maintenance Mode', 'hemelios' ),
			'subtitle' => esc_html__( 'Enable or disable the maintenance mode.', 'hemelios' ),
			'desc'     => '',
			'options'  => array( '2' => 'On (Custom Page)', '1' => 'On (Standard)', '0' => 'Off', ),
			'default'  => '0'
		),
		array(
			'id'       => 'maintenance_mode_page',
			'type'     => 'select',
			'data'     => 'pages',
			'required' => array( 'enable_maintenance', '=', '2' ),
			'title'    => esc_html__( 'Custom Maintenance Mode Page', 'hemelios' ),
			'subtitle' => esc_html__( 'Select the page that is your maintenance page, if you would like to show a custom page instead of the standard wordpress message. you should use the Holding Page template for this page.', 'hemelios' ),
			'desc'     => '',
			'default'  => '',
			'args'     => array()
		),
	),
));
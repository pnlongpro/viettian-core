<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */


$page_title_bg_url         = get_template_directory_uri() . '/assets/images/bg-page-title.jpg';

global $osOpt;

$osOpt->addSection(
	array(
		'title'      => esc_html__( 'Page Options', 'hemelios' ),
		'desc'       => '',
		'subsection' => true,
		'icon'       => 'el el-th-large',
		'fields'     => array(
			array(
				'id'       => 'page_layout',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Layout', 'hemelios' ),
				'subtitle' => esc_html__( 'Select page layout.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( 'full' => 'Full Width', 'container' => 'Container', 'container-fluid' => 'Container Fluid' ),
				'default'  => 'container'
			),
			array(
				'id'       => 'page_sidebar',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Sidebar', 'hemelios' ),
				'subtitle' => esc_html__( 'Set sidebar style.', 'hemelios' ),
				'desc'     => '',
				'options'  => array(
					'none'  => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-none.png' ),
					'left'  => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-left.png' ),
					'right' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-right.png' ),
					'both'  => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-both.png' ),
				),
				'default'  => 'right'
			),

			array(
				'id'       => 'page_sidebar_width',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Sidebar Width', 'hemelios' ),
				'subtitle' => esc_html__( 'Set sidebar width.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( 'small' => 'Small (1/4)', 'large' => 'Large (1/3)' ),
				'default'  => 'small',
				'required' => array( 'page_sidebar', '=', array( 'left', 'both', 'right' ) ),
			),


			array(
				'id'       => 'page_left_sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Left Sidebar', 'hemelios' ),
				'subtitle' => "Choose the default left sidebar.",
				'data'     => 'sidebars',
				'desc'     => '',
				'default'  => 'sidebar-1',
				'required' => array( 'page_sidebar', '=', array( 'left', 'both' ) ),
			),
			array(
				'id'       => 'page_right_sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Right Sidebar', 'hemelios' ),
				'subtitle' => "Choose the default right sidebar.",
				'data'     => 'sidebars',
				'desc'     => '',
				'default'  => 'sidebar-2',
				'required' => array( 'page_sidebar', '=', array( 'right', 'both' ) ),
			),

			array(
				'id'       => 'page_bottom_sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Bottom Sidebar', 'hemelios' ),
				'subtitle' => "Choose the default sidebar appear at the bottom of every page.",
				'data'     => 'sidebars',
				'desc'     => '',
				'default'  => '',
			),


			array(
				'id'       => 'show_page_title',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Show Page Title', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable page title.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '1'
			),

			array(
				'id'       => 'page_title_height',
				'type'     => 'dimensions',
				'units'    => 'px',
				'width'    => false,
				'title'    => esc_html__( 'Page Title Height', 'hemelios' ),
				'desc'     => esc_html__( 'You can set a height for the page title here.', 'hemelios' ),
				'required' => array( 'show_page_title', '=', array( '1' ) ),
				'default'  => array(
					'height' => '420'
				)
			),
			array(
				'id'       => 'breadcrumbs_in_page_title',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Page Breadcrumbs', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable page breadcrumbs in page title.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'required' => array( 'show_page_title', '=', array( '1' ) ),
				'default'  => '1'
			),
			array(
				'id'       => 'page_breadcrumbs_position',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Page Breadcrumbs Positions', 'hemelios' ),
				'subtitle' => esc_html__( 'Select page breadcrumbs positions.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '0' => 'Left', '1' => 'Center', '2' => 'Right' ),
				'required' => array( 'breadcrumbs_in_page_title', '=', array( '1' ) ),
				'default'  => '1'
			),

			array(
				'id'       => 'page_title_bg_image',
				'type'     => 'media',
				'url'      => true,
				'title'    => esc_html__( 'Page Title Background', 'hemelios' ),
				'subtitle' => esc_html__( 'Upload page title background.', 'hemelios' ),
				'desc'     => '',
				'default'  => array(
					'url' => $page_title_bg_url
				)
			),

			array(
				'id'       => 'page_content_mb',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Page Content Margin Bottom', 'hemelios' ),
				'subtitle' => esc_html__( 'Show or hide page content margin bottom.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'Yes', '0' => 'No' ),
				'default'  => '1'
			),
		)
	) );
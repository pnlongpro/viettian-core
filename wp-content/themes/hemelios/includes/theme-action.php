<?php
/*---------------------------------------------------
/* THEME URL REWRITE
/*---------------------------------------------------*/
if ( !function_exists( 'hemelios_less_css_url_rewrite' ) ) {
	function hemelios_less_css_url_rewrite() {
		//if (defined( 'HEMELIOS_SCRIPT_DEBUG' ) && HEMELIOS_SCRIPT_DEBUG) {
		add_rewrite_rule( 'wp-content/themes/hemelios/hemelios-less-css', 'index.php', 'top' );
		flush_rewrite_rules();
		//}
	}

	add_action( 'init', 'hemelios_less_css_url_rewrite' );
}

/*---------------------------------------------------
/* REMOVE ACTION
/*---------------------------------------------------*/
remove_action('hemelios_after_single_post_content','hemelios_author',15);
remove_action( 'hemelios_after_single_post_content', 'hemelios_post_nav', 20 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'hemelios_woocommerce_template_loop_category', 1 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 16 );

//remove_action( 'woocommerce_before_shop_loop_item_title', 'hemelios_woocomerce_template_loop_quick_view', 15 );
//remove_action( 'woocommerce_before_shop_loop_item_title','hemelios_woocomerce_template_loop_link',20);

//remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );


<?php
/**
 * Product Loop End
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
global $hemelios_woocommerce_loop;
?>
<?php if (isset($hemelios_woocommerce_loop['layout']) && ($hemelios_woocommerce_loop['layout'] == 'slider')) : ?>
	</div>
<?php endif; ?>
</div>
<?php hemelios_woocommerce_reset_loop(); ?>
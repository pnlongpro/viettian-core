<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/1/2015
 * Time: 8:51 AM
 */
if ( !defined( 'ABSPATH' ) ) {
	exit;// Exit if accessed directly
}
/*================================================
CORE FILES
================================================== */
include_once( get_template_directory() . '/hemelios-framework/xmenu/xmenu.php' );
include_once( get_template_directory() . '/hemelios-framework/tax-meta-class/Tax-meta-class.php' );
include_once( get_template_directory() . '/hemelios-framework/meta-box/meta-box.php' );
include_once( get_template_directory() . '/hemelios-framework/install-demo/install-demo.php' );
include_once( get_template_directory() . '/hemelios-framework/core/resize.php' );
include_once( get_template_directory() . '/hemelios-framework/core/action.php' );
include_once( get_template_directory() . '/hemelios-framework/core/wp-core.php' );
include_once( get_template_directory() . '/hemelios-framework/core/filter.php' );
include_once( get_template_directory() . '/hemelios-framework/core/base.php' );
include_once( get_template_directory() . '/hemelios-framework/core/breadcrumb.php' );
include_once( get_template_directory() . '/hemelios-framework/core/head.php' );
include_once( get_template_directory() . '/hemelios-framework/core/header.php' );
include_once( get_template_directory() . '/hemelios-framework/core/footer.php' );
include_once( get_template_directory() . '/hemelios-framework/core/blog.php' );
include_once( get_template_directory() . '/hemelios-framework/core/woocommerce.php' );

// Include 4k Icon plugin
include(get_template_directory() . "/hemelios-framework/4k-icons/4k-vc-icon-shortcode.php");
include(get_template_directory() . "/hemelios-framework/4k-icons/icons/4k-icons-pack01/4k-icon-pack.php");
include(get_template_directory() . "/hemelios-framework/4k-icons/icons/4k-icons-pack02/4k-icon-pack.php");
include(get_template_directory() . "/hemelios-framework/4k-icons/icons/4k-icons-pack03/4k-icon-pack.php");
include(get_template_directory() . "/hemelios-framework/4k-icons/icons/4k-icons-pack04/4k-icon-pack.php");
include(get_template_directory() . "/hemelios-framework/4k-icons/icons/4k-icons-pack05/4k-icon-pack.php");




$current_theme = get_template();

if ( ( $current_theme == 'hemelios' ) && !class_exists( 'hemeliosFrameWork' ) ) {
	class hemeliosFrameWork {

		protected $loader;

		protected $prefix;

		protected $version;

		public function __construct() {
			$this->version = '1.0.0';
			$this->prefix  = 'hemelios-framework';
			$this->define_constants();
			$this->includes();
			$this->define_hook();
		}

		private function define_constants() {
			//if( ! defined( 'PLUGIN_HEMELIOS_FRAMEWORK_DIR' ) ) {define( 'PLUGIN_HEMELIOS_FRAMEWORK_DIR', plugin_dir_path( dirname( __FILE__ ) ));}
			if ( !defined( 'PLUGIN_HEMELIOS_FRAMEWORK_DIR' ) ) {
				define( 'PLUGIN_HEMELIOS_FRAMEWORK_DIR', get_template_directory() .'/hemelios-framework/' );
			}
			if ( !defined( 'hemelios' ) ) {
				define( 'PLUGIN_HEMELIOS_FRAMEWORK_URI', get_template_directory_uri() .'/hemelios-framework/' );
			}
			if ( !defined( 'PLUGIN_HEMELIOS_FRAMEWORK_NAME' ) ) {
				define( 'PLUGIN_HEMELIOS_FRAMEWORK_NAME', 'hemelios-framework' );
			}
			if ( !defined( 'HEMELIOS_FRAMEWORK_SHORTCODE_CATEGORY' ) ) {
				define( 'HEMELIOS_FRAMEWORK_SHORTCODE_CATEGORY', __( 'Hemelios Shortcodes', 'hemelios' ) );
			}
		}

		private function includes() {
			require_once PLUGIN_HEMELIOS_FRAMEWORK_DIR . 'includes/class-hemelios-framework-loader.php';
			if ( !class_exists( 'WPAlchemy_MetaBox' ) ) {
				include_once( PLUGIN_HEMELIOS_FRAMEWORK_DIR . 'includes/MetaBox.php' );
			}
			require_once PLUGIN_HEMELIOS_FRAMEWORK_DIR . 'admin/class-hemelios-framework-admin.php';
			$this->loader = new hemeliosFramework_Loader();

			/*short-codes*/
			require_once PLUGIN_HEMELIOS_FRAMEWORK_DIR . 'includes/shortcodes/shortcodes.php';

			/* widgets */
			require_once PLUGIN_HEMELIOS_FRAMEWORK_DIR . 'includes/widgets/widgets.php';
		}

		private function define_hook() {
			/*set locale*/
			//$this->loader->add_action( 'plugins_loaded', $this, 'load_plugin_textdomain' );

			/*admin*/
			$plugin_admin = new HemeliosFramework_Admin( $this->get_prefix(), $this->get_version() );

			$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
			$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		}

		public function get_version() {
			return $this->version;
		}

		public function get_prefix() {
			return $this->prefix;
		}

		public function run() {
			$this->loader->run();
		}
	}

	if ( !function_exists( 'init_hemelios_framework' ) ) {
		function init_hemelios_framework() {
			$hemeliosFramework = new hemeliosFrameWork();
			$hemeliosFramework->run();
		}

		init_hemelios_framework();
	}
}

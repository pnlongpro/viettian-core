<?php
/**
 * Created by PhpStorm.
 * User: phuongth
 * Date: 6/10/15
 * Time: 11:47 AM
 */


/*================================================
BOTTOM BAR
================================================== */
if ( !function_exists( 'hemelios_bottom_bar_filter' ) ) {
	function hemelios_bottom_bar_filter() {
		hemelios_get_template( 'bottom-bar' );
	}

	add_action( 'hemelios_main_wrapper_footer', 'hemelios_bottom_bar_filter', 20 );
}

if ( !function_exists( 'hemelios_footer_widgets' ) ) {
	function hemelios_footer_widgets() {
		hemelios_get_template( 'footer-template' );
	}

	add_action( 'hemelios_main_wrapper_footer', 'hemelios_footer_widgets', 10 );
}

if ( !function_exists( 'hemelios_back_to_top' ) ) {
	function hemelios_back_to_top() {
		$hemelios_options = hemelios_option();
		$back_to_top = $hemelios_options['back_to_top'];
		if ( $back_to_top == 1 ) {
			hemelios_get_template( 'back-to-top' );
		}
	}

	add_action( 'hemelios_after_page_wrapper', 'hemelios_back_to_top', 5 );
}
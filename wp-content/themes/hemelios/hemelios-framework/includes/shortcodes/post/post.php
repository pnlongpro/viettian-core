<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/4/15
 * Time: 2:41 PM
 */
if (!defined('ABSPATH')) die('-1');
if (!class_exists('hemeliosFramework_Shortcode_Post')):
    class hemeliosFramework_Shortcode_Post
    {
        function __construct()
        {
            add_shortcode('os_post', array($this, 'post_shortcode'));
        }

        private function post_item($r, $class_col, $column = 0)
        {
            $counter = 0;
            while ($r->have_posts()) : $r->the_post(); ?>
                <div class="<?php echo esc_attr($class_col); ?>">
                    <div class="os-post-item">
                        <?php
                        $thumbnail = hemelios_post_thumbnail('blog-thumbnail');
                        if (!empty($thumbnail)) : ?>
                            <div class="os-post-image">
                                <?php echo wp_kses_post($thumbnail); ?>
                            </div>
                        <?php endif; ?>
                        <div class="os-post-content">
                            <a class="os-post-title" href="<?php the_permalink(); ?>"
                               rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                            <div class="entry-meta">
                                <span><?php the_time('F j, Y'); ?></span>
                                <span><?php the_author(); ?></span>
                                <span><?php the_category(', '); ?></span>
                            </div>
                            <?php
                            $excerpt = get_the_excerpt();
                            $excerpt = hemeliosFramework_Shortcodes::substr($excerpt, 110, '...');
                            ?>
                            <p><?php echo($excerpt); ?></p>
                        </div>
                    </div>
                </div>
                <?php if(++$counter == $column){ echo('<div class="col-xs-12 hidden-xs"></div>');} ?>
                <?php
            endwhile;
        }

        function post_shortcode($atts)
        {
           $post_style = $title = $title_style = $display = $column = $is_slider = $item_amount = $html = $el_class = $hemelios_animation = $css_animation = $duration = $delay = $styles_animation = '';
            extract(shortcode_atts(array(
                'post_style' =>'style1',
                'title' => '',
                'title_style' => 'border-bottom',
                'display' => 'recent',
                'item_amount' => '10',
                'is_slider' => false,
                'column' => '3',
                'el_class' => '',
                'css_animation' => '',
                'duration' => '',
                'delay' => ''
            ), $atts));

            $hemelios_animation .= ' ' . esc_attr($el_class);
            $hemelios_animation .= hemeliosFramework_Shortcodes::hemelios_get_css_animation($css_animation);

            $query['posts_per_page'] = $item_amount;
            $query['no_found_rows'] = true;
            $query['post_status'] = 'publish';
            $query['ignore_sticky_posts'] = true;
            $query['post_type'] = 'post';
            $query['tax_query'] = array(
                array(
                    'taxonomy' => 'post_format',
                    'field' => 'slug',
                    'terms' => array('post-format-quote', 'post-format-link', 'post-format-audio'),
                    'operator' => 'NOT IN'
                )
            );
            if ($display == 'random') {
                $query['orderby'] = 'rand';
            } elseif ($display == 'popular') {
                $query['orderby'] = 'comment_count';
            } elseif ($display == 'recent') {
                $query['orderby'] = 'post_date';
                $query['order'] = 'DESC';
            } else {
                $query['orderby'] = 'post_date';
                $query['order'] = 'ASC';
            }
            $r = new WP_Query($query);
            ob_start();
            $class_col = 'col-lg-' . (12 / esc_attr($column)) . ' col-md-' . (12 / esc_attr($column)) . ' col-sm-6 col-xs-12 common-post-wrapper';
            $counter = 0;
            if ($r->have_posts()) :
                ?>
                <div
                    class="os-post <?php echo esc_attr($hemelios_animation) ?> <?php echo esc_attr($post_style) ?>" <?php echo hemeliosFramework_Shortcodes::hemelios_get_style_animation($duration, $delay); ?>>
                    <?php if ($title) : ?>
                        <h3 class="os-title <?php echo esc_attr($title_style) ?>"><?php echo esc_html($title) ?></h3>
                    <?php endif; ?>
                    <?php if ($is_slider) : ?>
                        <div class="os-post-slider">
                            <div
                                data-plugin-options='{"items" : <?php echo esc_attr($column) ?>,"itemsDesktop":[1199, 3],"itemsDesktopSmall":[980, 2],"itemsTablet":[768, 1],"pagination":true,"navigation":false, "autoPlay": true }'
                                class="owl-carousel">
                                <?php $this->post_item($r, 'carousel-post-wrapper'); ?>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="row">
                            <?php
                                $this->post_item($r, $class_col, $column);
                            ?>
                        </div>
                    <?php endif; ?>
                </div>
                <?php
            endif;
            wp_reset_postdata();
            hemelios_archive_loop_reset();
            $content = ob_get_clean();
            return $content;
        }
    }

    new hemeliosFramework_Shortcode_Post();
endif;
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 11/7/2015
 * Time: 4:43 PM
 */
// don't load directly
if ( !defined( 'ABSPATH' ) ) {
	die( '-1' );
}
if ( !class_exists( 'hemeliosFramework_Shortcode_List' ) ) {
	class hemeliosFramework_Shortcode_List {
		function __construct() {
			add_shortcode( 'hemelios_list', array( $this, 'list_shortcode' ) );
		}

		function list_shortcode( $atts ) {
			/**
			 * Shortcode attributes
			 * @var $layout_style
			 * @var $title
			 * @var $color
			 * @var $custom_color
			 * @var $values_general
			 * @var $values_separate
			 * @var $icon_type
			 * @var $icon_image
			 * @var $el_class
			 * @var $css_animation
			 * @var $duration
			 * @var $delay
			 */
			$color_style = $iconClass = $layout_style = $title = $color = $custom_color = $values_separate = $values_general = $icon_type = $icon_image = $el_class = $css_animation = $duration = $delay = '';
			$atts        = vc_map_get_attributes( 'hemelios_list', $atts );
			$list_id = RandomString( 20 );
			extract( $atts );
			global $hemelios_options;
			$min_suffix_css = ( isset( $hemelios_options['enable_minifile_css'] ) && $hemelios_options['enable_minifile_css'] == 1 ) ? '.min' : '';
			//wp_enqueue_style('hemelios_list_css', PLUGIN_HEMELIOS_FRAMEWORK_URI . 'includes/shortcodes/list/assets/css/list' . $min_suffix_css . '.css', array(), false);
			$g5plus_animation = ' ' . esc_attr( $el_class ) . hemeliosFramework_Shortcodes::hemelios_get_css_animation( $css_animation );
			$color_css        = array();
			if ( $color == 'custom-color' ) {
				if ( $custom_color != '' ) {
					$color_css[] = 'color: ' . $custom_color . '';
				}
			}
			if ( $color_css ) {
				$color_style = 'style="' . join( ';', $color_css ) . '"';
			}
			ob_start();
			?>
			<ul class="list <?php echo esc_attr( $layout_style . ' ' . $color . $g5plus_animation ) ?>" <?php echo hemeliosFramework_Shortcodes::hemelios_get_style_animation( $duration, $delay ); ?>>
				<?php if ( $layout_style != 'style6' ):
					$number_list = 1;
					$values_general = (array) vc_param_group_parse_atts( $values_general );
					foreach ( $values_general as $data ) {
						$title = isset( $data['title'] ) ? $data['title'] : '';
						if ( !empty( $title ) ):?>
							<li class="list-item">
							<span class="list-before-title p-font" <?php echo $color_style; ?>>
								<?php if ( $layout_style == 'style3' ): ?>
									<?php echo esc_attr( $number_list . '.' ) ?>
								<?php endif;
								if ( $layout_style == 'style4' || $layout_style == 'style5' ): ?>
									<?php if ( $number_list < 10 ) echo esc_attr( '0' . $number_list ) ?>
									<?php if ( $number_list >= 10 ) echo esc_attr( $number_list ) ?>
								<?php endif; ?>
							</span>
							<span class="list-title">
								<?php echo esc_html( $title ) ?>
							</span>
							</li>
						<?php endif;
						$number_list ++;
					}
				endif;
				if ( $layout_style == 'style6' ):
					$values_separate = (array) vc_param_group_parse_atts( $values_separate );
					foreach ( $values_separate as $data ) {
						$iconClass = '';
						$title     = isset( $data['title'] ) ? $data['title'] : '';
						$icon_type = isset( $data['icon_type'] ) ? $data['icon_type'] : '';
						if ( $icon_type != '' && $icon_type != 'image' ) {
							vc_icon_element_fonts_enqueue( $icon_type );
							$iconClass = isset( $data['icon'] ) ? esc_attr( $data['icon'] ) : '';
						}
						if ( !empty( $iconClass ) ) {
							// Don't load the CSS files to trim loading time, include the specific styles via PHP
							// wp_enqueue_style( '4k-icon-' . $cssFile, plugins_url( 'icons/css/' . $cssFile . '.css', __FILE__ ) );
							$cssFile = substr( $iconClass, 0, stripos( $iconClass, '-' ) );
							wp_enqueue_style( '4k-icons', PLUGIN_HEMELIOS_FRAMEWORK_DIR . '4k-icons/css/icon-styles.css', null, 2.9 );
							wp_enqueue_script( '4k-icons', PLUGIN_HEMELIOS_FRAMEWORK_DIR . '4k-icons/js/script-ck.js', array( 'jquery' ), 2.9, true );
						}
						global $iconContents;
						include( PLUGIN_HEMELIOS_FRAMEWORK_DIR . 'includes/shortcodes/icon-contents.php' );
						// Normal styles used for everything
						$cssFile = substr( $iconClass, 0, stripos( $iconClass, '-' ) );

						$iconFile = PLUGIN_HEMELIOS_FRAMEWORK_DIR . '4k-icons/icons/fonts/' . $cssFile;
						$iconFile = apply_filters( '4k_icon_font_pack_path', $iconFile, $cssFile );

						// Fix ligature icons (these are icons that use more than 1 symbol e.g. mono social icons)
						$ligatureStyle = '';
						if ( $cssFile == 'mn' ) {
							$ligatureStyle = '-webkit-font-feature-settings:"liga","dlig";-moz-font-feature-settings:"liga=1, dlig=1";-moz-font-feature-settings:"liga","dlig";-ms-font-feature-settings:"liga","dlig";-o-font-feature-settings:"liga","dlig";
                         	 font-feature-settings:"liga","dlig";
                        	 text-rendering:optimizeLegibility;';
						}
						$iconCode = '';
						if ( !empty( $data['icon'] ) ) {
							$iconCode = $iconContents[$data['icon']];
						}
						$ret = "<style>
            @font-face {
            	font-family: '" . $cssFile . "';
            	src:url('" . $iconFile . ".eot');
            	src:url('" . $iconFile . ".eot?#iefix') format('embedded-opentype'),
            		url('" . $iconFile . ".woff') format('woff'),
            		url('" . $iconFile . ".ttf') format('truetype'),
            		url('" . $iconFile . ".svg#oi') format('svg');
            	font-weight: normal;
            	font-style: normal;
            }
            #rand_" . $list_id . " ." . $iconClass . ":before { font-family: '" . $cssFile . "'; font-weight: normal; font-style: normal; }
            #rand_" . $list_id . " ." . $iconClass . ":before { content: \"" . $iconCode . "\"; $ligatureStyle }
";

						$ret .= "</style>";

						// Compress styles a bit for readability
						$ret = preg_replace( "/\s?(\{|\})\s?/", "$1",
								preg_replace( "/\s+/", " ",
									str_replace( "\n", "", $ret ) ) )
							. "\n";

						echo $ret;

						if ( $icon_type == 'image' ) {
							$icon_image = isset( $data['icon_image'] ) ? esc_attr( $data['icon_image'] ) : '';
						}
						if ( !empty( $title ) ):?>
							<li class="list-item">
							<span class="list-before-title">
								<?php if ( $icon_type != '' ) :
									if ( $icon_type == 'image' ) :
										$img = wp_get_attachment_image_src( $icon_image, 'full' ); ?>
										<img src="<?php echo esc_url( $img[0] ) ?>" />
									<?php else : ?>
										<i class="<?php echo esc_attr( $iconClass ) ?>"></i>
									<?php endif;
								endif; ?>
							</span>
							<span class="list-title">
								<?php echo esc_html( $title ) ?>
							</span>
							</li>
							<?php
						endif;
					}
				endif; ?>

			</ul>
			<?php
			$content = ob_get_clean();

			return $content;
		}
	}

	new hemeliosFramework_Shortcode_List();
}
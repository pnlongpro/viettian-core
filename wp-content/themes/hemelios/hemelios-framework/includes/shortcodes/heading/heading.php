<?php
// don't load directly
if (!defined('ABSPATH')) die('-1');
if (!class_exists('hemeliosFramework_Shortcode_Heading')) {
	class hemeliosFramework_Shortcode_Heading
	{
		function __construct()
		{
			add_shortcode('os_heading', array($this, 'heading_shortcode'));
		}

		function heading_shortcode($atts)
		{
			$align = $title_style = $font_size_title = $title = $layout_style = $title = $html = $el_class = $hemelios_animation = $css_animation = $duration = $delay = $styles_animation = '';
			extract(shortcode_atts(array(
				'layout_style' => 'style1',
				'title' => '',
				'title_style' => 'no-border',
				'font_size_title' => '18',
				'align' => 'left',
				'el_class' => '',
				'css_animation' => '',
				'duration' => '',
				'delay' => ''
			), $atts));
			$custom_styles = array();
			$hemelios_animation .= ' ' . esc_attr($el_class);
			$hemelios_animation .= hemeliosFramework_Shortcodes::hemelios_get_css_animation($css_animation);
			if( $font_size_title != '' ){
				$custom_styles[] = 'font-size:' . $font_size_title.'px';
			}
			$style= '';
			if ($custom_styles) {
				$style = 'style="'. join(';',$custom_styles).'"';
			}
			if( $title_style == 'border-bottom' ){
				$title_class = 'border-bottom';
			}else{
				$title_class = 'no-border';
			}
			ob_start();?>
			<div class="os-heading <?php echo esc_attr($title_class); ?> <?php  echo esc_attr($align) ?> <?php echo esc_attr($layout_style) ?><?php echo esc_attr($hemelios_animation) ?>" <?php echo hemeliosFramework_Shortcodes::hemelios_get_style_animation($duration, $delay); ?>>
				<?php if ($title != ''): ?>
					<h2 <?php echo wp_kses_post($style); ?>><?php echo wp_kses_post($title) ?></h2>
				<?php endif; ?>
			</div>
			<?php
			$content = ob_get_clean();
			return $content;
		}
	}

	new hemeliosFramework_Shortcode_Heading();
}
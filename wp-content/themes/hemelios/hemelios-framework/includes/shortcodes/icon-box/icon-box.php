<?php
// don't load directly
if (!defined('ABSPATH')) die('-1');
if (!class_exists('hemeliosFramework_Shortcode_Icon_Box')) {
	class hemeliosFramework_Shortcode_Icon_Box
	{
		function __construct()
		{
			add_shortcode('os_icon_box', array($this, 'icon_box_shortcode'));
		}

		function icon_box_shortcode($atts)
		{
			$title_style = $icon_type = $image = $color_style = $icon_style = $layout_style = $link = $description = $title = $symbol_bg =
			$icon = $html = $el_class =  $style_icon =
			$hemelios_animation = $css_animation = $duration = $delay = $styles_animation = '';
			extract(shortcode_atts(array(
				'layout_style' => 'istyle-left',
				'color_style' => 'istyle-inherit',
				'icon_type' => 'font-icon',
				'icon' => '',
				'icon_style' => 'simple-icon',
				'image' => '',
				'link' => '',
				'title' => '',
				'title_style' => 'no-border',
				'description' => '',
				'symbol_bg' => '',
				'el_class' => '',
				'css_animation' => '',
				'duration' => '',
				'delay' => ''
			), $atts));
			$custom_data = array();
			$hemelios_animation .= ' ' . esc_attr($el_class);
			$hemelios_animation .= hemeliosFramework_Shortcodes::hemelios_get_css_animation($css_animation);

			//parse link
			$link = ($link == '||') ? '' : $link;
			$link = vc_build_link($link);

            $prelink = $endlink = '';
			if( $title_style == 'border-bottom' ){
				$title_class = 'border-bottom';
			}else{
				$title_class = 'no-border';
			}
			if (strlen($link['url']) > 0) {
				$a_href = $link['url'];
				$a_title = $link['title'];
				$a_target = strlen($link['target']) > 0 ? $link['target'] : '_self';
                $prelink = '<a title="'.esc_attr($a_title).'" target="'.trim(esc_attr($a_target)).'" href="'.esc_url($a_href).'">';
                $endlink = '</a>';
			}

			ob_start();?>
			<div class="os-ibox clearfix <?php echo esc_attr($layout_style) . ' ' . esc_attr($color_style); ?> <?php echo esc_attr($hemelios_animation)?>"
				<?php echo hemeliosFramework_Shortcodes::hemelios_get_style_animation($duration, $delay); ?>>

				<div class="__header clearfix">
					<div class="__icon <?php echo esc_attr($icon_style) ?>">
                        <?php echo $prelink ?>
                            <?php if ($icon_type == 'font-icon') : ?>
                                <i class="<?php echo esc_attr($icon) ?>"></i>
                                <?php
                            else :
                                $img = wp_get_attachment_image_src($image, 'large');
                                ?>
                                <img src="<?php echo esc_attr($img[0]) ?>"/>
                            <?php endif; ?>
                        <?php echo $endlink ?>
					</div>
					<?php if ($title != ''): ?>
						<h3 class="__title <?php echo esc_attr($title_class); ?>">
							<?php echo $prelink ?>
								<?php echo esc_html($title) ?>
							<?php echo $endlink ?>
						</h3>
					<?php endif;?>
				</div>

				<div class="__content">
					<?php if ($description != ''):?>
						<p><?php echo wp_kses_post($description) ?></p>
					<?php endif; ?>
				</div>

				<?php if($symbol_bg != ''): ?>
					<div class="__symbol-bg"><?php echo esc_html($symbol_bg) ;?></div>
				<?php endif ?>

			</div>

			<?php
			$content = ob_get_clean();
			return $content;
		}
	}

	new hemeliosFramework_Shortcode_Icon_Box();
}
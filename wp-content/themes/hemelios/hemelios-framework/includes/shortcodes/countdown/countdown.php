<?php
if ( ! defined( 'ABSPATH' ) ) die( '-1' );
if(!class_exists('os_Countdown')){
    class os_Countdown {
        function __construct() {
           // add_action( 'init', array($this, 'register_post_types' ), 6 );
            //add_shortcode('os_countdown_shortcode', array($this, 'os_countdown_shortcode' ));
            //add_filter( 'rwmb_meta_boxes', array($this,'os_register_meta_boxes' ));
        }

        function register_post_types() {
            if ( post_type_exists('countdown') ) {
                return;
            }
            register_post_type('countdown',
                array(
                    'label' => __('Countdown','hemelios'),
                    'description' => __( 'Countdown Description', 'hemelios' ),
                    'labels' => array(
                        'name'					=>'Countdown',
                        'singular_name' 		=> 'Countdown',
                        'menu_name'    			=> __( 'Countdown', 'hemelios' ),
                        'parent_item_colon'  	=> __( 'Parent Item:', 'hemelios' ),
                        'all_items'          	=> __( 'All Countdown', 'hemelios' ),
                        'view_item'          	=> __( 'View Item', 'hemelios' ),
                        'add_new_item'       	=> __( 'Add New Countdown', 'hemelios' ),
                        'add_new'            	=> __( 'Add New', 'hemelios' ),
                        'edit_item'          	=> __( 'Edit Item', 'hemelios' ),
                        'update_item'        	=> __( 'Update Item', 'hemelios' ),
                        'search_items'       	=> __( 'Search Item', 'hemelios' ),
                        'not_found'          	=> __( 'Not found', 'hemelios' ),
                        'not_found_in_trash' 	=> __( 'Not found in Trash', 'hemelios' ),
                    ),
                    'supports'    => array( 'title', 'editor', 'comments', 'thumbnail'),
                    'public'      => true,
                    'has_archive' => true
                )
            );
        }

        function os_countdown_shortcode($atts){
            $type = $css = '';
            extract( shortcode_atts( array(
                'type'     => '',
                'css'      => ''
            ), $atts ) );

            $plugin_path =  untrailingslashit( plugin_dir_path( __FILE__ ) );
            $template_path = $plugin_path . '/templates/'.$type.'.php';
            ob_start();
            include($template_path);
            $ret = ob_get_contents();
            ob_end_clean();
            return $ret;
        }

        function os_register_meta_boxes($meta_boxes){
            $meta_boxes[] = array(
                'title'  => __( 'Countdown Option', 'hemelios' ),
                'id'     => 'hemelios-meta-box-countdown-opening',
                'pages'  => array( 'countdown' ),
                'fields' => array(
                    array(
                        'name' => __( 'Opening hours', 'hemelios' ),
                        'id'   => 'countdown-opening',
                        'type' => 'datetime',
                    ),
                     array(
                         'name' => __( 'Type', 'hemelios' ),
                         'id'   => 'countdown-type',
                         'type' => 'select',
                         'options'  => array(
                             'comming-soon' => __('Coming Soon','hemelios'),
                             'under-construction' => __('Under Construction','hemelios')
                         )
                     ),
                    array(
                        'name' => __( 'Url redirect (after countdown completed)', 'hemelios' ),
                        'id'   => 'countdown-url',
                        'type' => 'textarea',
                    ),
                    array(
                        'name' => __('Footer text', 'hemelios'),
                        'id' => 'footer_text',
                        'type' => 'textarea',
                    )
                )
            );
            return $meta_boxes;
        }
    }
    new os_Countdown();
}
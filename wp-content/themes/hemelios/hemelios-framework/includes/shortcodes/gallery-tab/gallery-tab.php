<?php
// don't load directly
if ( !defined( 'ABSPATH' ) ) {
	die( '-1' );
}
if ( !class_exists( 'HemeliosFramework_Shortcode_Gallery_Tab' ) ) {
	class HemeliosFramework_Shortcode_Gallery_Tab {
		function __construct() {
			add_shortcode( 'hemelios_gallery_tab', array( $this, 'hemelios_gallery_tab_shortcode' ) );
		}

		function hemelios_gallery_tab_shortcode( $atts, $content ) {
			$layout_style =
			$gallery_items =
			$column_number =
			$title =
			$images_gallery =
			$gallery_class =
			$el_class =
			$hemelios_animation =
			$css_animation =
			$duration =
			$delay = '';
			extract( shortcode_atts( array(
				'layout_style'  => 'style1',
				'column_number' => '3',
				'gallery_items' => '',
				'el_class'      => '',
				'css_animation' => '',
				'duration'      => '',
				'delay'         => ''
			), $atts ) );
			$data_section_id = uniqid();
			$hemelios_animation .= ' ' . esc_attr( $el_class );
			$hemelios_animation .= HemeliosFramework_Shortcodes::hemelios_get_css_animation( $css_animation );
			$gallery_items = vc_param_group_parse_atts( $atts['gallery_items'] );
			if ( $column_number != '' ) {
				$gallery_class = 'col-' . $column_number . '';
			}
			ob_start(); ?>
			<div class="hemelios-galerry-tabs <?php echo esc_attr( $hemelios_animation ) ?> <?php echo esc_attr( $layout_style ) ?>" <?php echo hemeliosFramework_Shortcodes::hemelios_get_style_animation( $duration, $delay ); ?> id="gallery-tab-<?php echo esc_attr( $data_section_id ) ?>">
				<div class="gallery-tabs-wrapper">
					<div class="tab-wrapper">
						<ul class="cat-style-normal primary-color">
							<?php
							foreach ( $gallery_items as $value ) :
								?>
								<li >
									<a class="isotope-gallery ladda-button " href="javascript:;" data-section-id="<?php echo esc_attr( $data_section_id ) ?>" data-filter=".<?php echo sanitize_title( $value['title'] ) ?>">
										<?php echo esc_html( $value['title'] ) ?> </a>
								</li>
								<?php
							endforeach;
							?>
						</ul>
					</div>

				</div>
				<div class="os-gallery <?php echo esc_attr( $gallery_class ) ?>" data-section-id="<?php echo esc_attr( $data_section_id ) ?>" id="gallery-container-<?php echo esc_attr( $data_section_id ) ?>" data-columns="<?php echo esc_attr( $column_number ) ?>">
					<?php
					foreach ( $gallery_items as $key => $value ) :
						$image_array = explode( ",", $value['images_gallery'] );
						if ( $image_array ) :
							foreach ( $image_array as $image ) :
								$thumbnail_url = '';
								$width = 390;
								$height = 260;
								if ( !empty( $image ) ) {
									$images_attr = wp_get_attachment_image_src( $image, "full" );
									if ( isset( $images_attr ) ) {
										$resize = matthewruddy_image_resize( $images_attr[0], $width, $height );
										if ( $resize != null ) {
											$thumbnail_url = $resize['url'];
										}
									}
								}
								?>

								<div class="gallery-item hemelios-gallary-thumb <?php echo sanitize_title( $value['title'] ) ?>">
									<div class="gallery-item-wrapper">
										<div class="gallery-inner-thumb">
											<img alt="" src="<?php echo esc_url( $thumbnail_url ); ?>">
											<div class="gallery-hover">
												<a data-rel="prettyPhoto[feature-<?php echo sanitize_title($value['title']); ?> ]" href="<?php echo esc_url( $images_attr[0] ); ?>">
													<i class="fa fa-search"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
								<?php
							endforeach;
						endif;

					endforeach;
					?>
				</div>
			</div>

			<script type="text/javascript">
				(function ($) {
					"use strict";
					var $container = jQuery('div[data-section-id="<?php echo esc_attr( $data_section_id ); ?>"]');
					$($container).css('opacity', 0);
					$(window).load(function () {
//						$('os-gallery').isotope({
//							filter: 'LớpA'
//						});
						$('.isotope-gallery', '.gallery-tabs-wrapper').off();
						$('.isotope-gallery', '.gallery-tabs-wrapper').click(function () {
							$('.isotope-gallery', '.gallery-tabs-wrapper').removeClass('active');
							$('li', '.gallery-tabs-wrapper').removeClass('active');
							$(this).addClass('active');
							$(this).parent().addClass('active');
							var dataSectionId = $(this).attr('data-section-id');
							var filter = $(this).attr('data-filter');
							var $container = jQuery('div[data-section-id="' + dataSectionId + '"]').isotope({filter: filter});
							$container.imagesLoaded(function () {
								$container.isotope('layout');
							});
						});

						var $container = jQuery('div[data-section-id="<?php echo esc_attr( $data_section_id ); ?>"]');
						var $firstItem = $('li', '.gallery-tabs-wrapper').first().find('a');
						$firstItem.addClass('active');
						var $firstFilter = $firstItem.attr('data-filter');
						$container.isotope({filter: $firstFilter});
						$container.imagesLoaded(function () {
							$container.isotope('layout');
						});

						$($container).css('opacity', 1);
					});

				})(jQuery);
			</script>

			<?php
			$output = ob_get_clean();

			return $output;
		}

	}

	new HemeliosFramework_Shortcode_Gallery_Tab();
}

?>

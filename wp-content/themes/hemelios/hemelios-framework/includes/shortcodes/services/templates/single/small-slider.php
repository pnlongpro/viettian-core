<?php

do_action('hemelios_before_page');

$data_section_id = uniqid();

$terms = wp_get_post_terms( get_the_ID(), array( HEMELIOS_PORTFOLIO_CATEGORY_TAXONOMY));
$cat = $cat_filter = '';
foreach ( $terms as $term ){
	$cat_filter .= preg_replace('/\s+/', '', $term->name) .' ';
	$cat .= $term->name.', ';
}
$cat = rtrim($cat,', ');

?>
<?php
global $hemelios_options;

$layout_style = hemelios_get_post_meta($post->ID, 'hemelios_page_layout', true);
if (($layout_style === '') || ($layout_style == '-1')) {
	$layout_style = $hemelios_options['service_layout'];
}

$sidebar = hemelios_get_post_meta($post->ID, 'hemelios_page_sidebar', true);
if (($sidebar === '') || ($sidebar == '-1')) {
	$sidebar = $hemelios_options['service_sidebar'];
}

$left_sidebar = hemelios_get_post_meta($post->ID, 'hemelios_page_left_sidebar', true);
if (($left_sidebar === '') || ($left_sidebar == '-1')) {
	$left_sidebar = $hemelios_options['service_left_sidebar'];

}

$right_sidebar = hemelios_get_post_meta($post->ID, 'hemelios_page_right_sidebar', true);
if (($right_sidebar === '') || ($right_sidebar == '-1')) {
	$right_sidebar = $hemelios_options['service_right_sidebar'];
}

$sidebar_width = hemelios_get_post_meta($post->ID, 'hemelios_sidebar_width', true);
if (($sidebar_width === '') || ($sidebar_width == '-1')) {
	$sidebar_width = $hemelios_options['service_sidebar_width'];
}

// Calculate sidebar column & content column
$sidebar_col = 'col-md-3';
if ($sidebar_width == 'large') {
	$sidebar_col = 'col-md-4';
}

$content_col_number = 12;
if (is_active_sidebar($left_sidebar) && (($sidebar == 'both') || ($sidebar == 'left'))) {
	if ($sidebar_width == 'large') {
		$content_col_number -= 4;
	} else {
		$content_col_number -= 3;
	}
}
if (is_active_sidebar($right_sidebar) && (($sidebar == 'both') || ($sidebar == 'right'))) {
	if ($sidebar_width == 'large') {
		$content_col_number -= 4;
	} else {
		$content_col_number -= 3;
	}
}

$content_col = 'col-md-' . $content_col_number;
if (($content_col_number == 12) && ($layout_style == 'full')) {
	$content_col = '';
}
$main_class = array('site-content-page');

if ($content_col_number < 12) {
	$main_class[] = 'has-sidebar';
}
?>
<main class="<?php echo join(' ',$main_class) ?>">
	<?php if ($layout_style != 'full'): ?>
	<div class="<?php echo esc_attr($layout_style) ?> clearfix">
		<?php endif;?>
		<?php if (($content_col_number != 12) || ($layout_style != 'full')): ?>
		<div class="row clearfix">
			<?php endif;?>
			<?php if (is_active_sidebar( $left_sidebar ) && (($sidebar == 'left') || ($sidebar == 'both'))): ?>
				<div class="sidebar left-sidebar <?php echo esc_attr($sidebar_col) ?>">
					<?php dynamic_sidebar( $left_sidebar );?>
				</div>
			<?php endif;?>
			<div class="site-content-page-inner <?php echo esc_attr($content_col) ?>">
				<div class="page-content">
					<h3 class="os-title"><?php the_title(); ?></h3>
					<?php if( isset($meta_values) && !empty($meta_values) ){ ?>
					<div class="services-slideshow" id="services_slideshow_<?php echo esc_attr($data_section_id) ?>">
						<div id="services-slider" class="portfolio-single-slider">
							<?php
							$index = 0;
							foreach($meta_values as $image){
								$urls = wp_get_attachment_image_src($image,'full');
								$img = '';
								if($urls){
									$resize = matthewruddy_image_resize($urls[0],870,434);
									if($resize!=null )
										$img = $resize['url'];
								}
								?>
							<div>
								<a href="<?php echo esc_url($img) ?>" data-index="<?php echo esc_attr($image) ?>" data-rel="prettyPhoto[portfolio-gallery]" >
									<img  alt="portfolio" src="<?php echo esc_url($img) ?>" />
								</a>
							</div>
							<?php } ?>
						</div>
						<div id="services-carousel" class="owl-carousel manual portfolio-single-slider">
							<?php
							foreach($meta_values as $image){
								$urls = wp_get_attachment_image_src($image,'full');
								$img = '';
								if($urls){
									$resize = matthewruddy_image_resize($urls[0],170,133);
									if($resize!=null )
										$img = $resize['url'];
								}
								?>
								<div class="portfolio-thumbnail">
									<a href="<?php echo esc_url($img) ?>" data-index="<?php echo esc_attr($image) ?>"  >
										<img  alt="portfolio" src="<?php echo esc_url($img) ?>" />
									</a>
								</div>
							<?php } ?>
						</div>
						<?php
						}else { if(count($imgThumbs)>0) {?>
							<div class="item"><img alt="portfolio" src="<?php echo esc_url($imgThumbs[0])?>" /></div>
						<?php }
						}
						?>
					</div>
					<div class="service-content">
						<?php the_content() ?>
					</div>
				</div>
			</div>
			<?php if (is_active_sidebar( $right_sidebar ) && (($sidebar == 'right') || ($sidebar == 'both'))): ?>
				<div class="sidebar right-sidebar <?php echo esc_attr($sidebar_col) ?>">
					<?php dynamic_sidebar( $right_sidebar );?>
				</div>
			<?php endif;?>
			<?php if (($content_col_number != 12) || ($layout_style != 'full')): ?>
		</div>
	<?php endif;?>
		<?php if ($layout_style != 'full'): ?>
	</div>
<?php endif;?>

</main>
<?php
if( is_active_sidebar('sidebar-services-bottom') ){
	?>
	<div class="bottom-single-service sidebar">
		<?php dynamic_sidebar('sidebar-services-bottom'); ?>
	</div>
	<?php
}
?>



<script type="text/javascript">
	(function ($) {
		"use strict";
		$(document).ready(function () {

			var sync1 = $("#services-slider", ".services-slideshow");
			var sync2 = $("#services-carousel", ".services-slideshow");

			sync1.owlCarousel({
				singleItem           : true,
				slideSpeed           : 100,
				navigation           : true,
				navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
				pagination           : false,
				afterAction          : syncPosition,
				responsiveRefreshRate: 200
			});

			sync2.owlCarousel({
				items                : 4,
				itemsDesktop         : [1199, 4],
				itemsDesktopSmall    : [980, 4],
				itemsTablet          : [768, 4],
				itemsTabletSmall     : false,
				itemsMobile          : [479, 2],
				pagination           : false,
				responsiveRefreshRate: 100,
				navigation           : false,
				//navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
				afterInit            : function (el) {
					el.find(".owl-item").eq(0).addClass("synced");
				},
				afterMove: function (elem) {
					var current = this.currentItem;
					var src = elem.find(".owl-item").eq(current).find("img").attr('src');
					console.log('Image current is ' + src);
				}
			});
			function syncPosition(el) {
				var current = this.currentItem;
				$("#services-carousel")
						.find(".owl-item")
						.removeClass("synced")
						.eq(current)
						.addClass("synced");
				if ($("#services-carousel").data("owlCarousel") !== undefined) {
				}
			}

			$("#services-carousel").on("click", ".owl-item", function (e) {
				e.preventDefault();
				var number = $(this).data("owlItem");

				sync1.trigger("owl.goTo", number);
			});

			function center(number) {
				var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
				var num = number;
				var found = false;
				for (var i in sync2visible) {
					if (num === sync2visible[i]) {
						var found = true;
					}
				}

				if (found === false) {
					if (num > sync2visible[sync2visible.length - 1]) {
						sync2.trigger("owl.goTo", num - sync2visible.length + 2)
					} else {
						if (num - 1 === -1) {
							num = 0;
						}
						sync2.trigger("owl.goTo", num);
					}
				} else if (num === sync2visible[sync2visible.length - 1]) {
					sync2.trigger("owl.goTo", sync2visible[1])
				} else if (num === sync2visible[0]) {
					sync2.trigger("owl.goTo", num - 1)
				}
			}
		});
	})(jQuery);
</script>
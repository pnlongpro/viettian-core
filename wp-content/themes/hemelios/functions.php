<?php

if (!function_exists('hemelios_include_theme_options')) {
	function hemelios_include_theme_options() {

		if (!class_exists('ReduxFramework')) {
			require_once (get_template_directory() . '/hemelios-framework/options/framework.php');
		}
		require_once (get_template_directory() . '/hemelios-framework/option-extensions/loader.php');
		require_once (get_template_directory() . '/includes/options-config.php');

		global $hemelios_hemelios_options, $hemelios_options;
		$hemelios_options = $hemelios_hemelios_options;
	}

	hemelios_include_theme_options();
}

function hemelios_check_vc_status() {
	if (function_exists('vc_is_as_theme')) {
		return true;
	} else {
		return false;
	}
}

if (!function_exists('hemelios_include_library')) {
	function hemelios_include_library() {
		require_once (get_template_directory() . '/hemelios-framework/hemelios-framework.php');
		require_once (get_template_directory() . '/includes/register-require-plugin.php');
		require_once (get_template_directory() . '/includes/theme-setup.php');
		require_once (get_template_directory() . '/includes/sidebar.php');
		require_once (get_template_directory() . '/includes/meta-boxes.php');
		require_once (get_template_directory() . '/includes/admin-enqueue.php');
		require_once (get_template_directory() . '/includes/theme-functions.php');
		require_once (get_template_directory() . '/includes/theme-action.php');
		require_once (get_template_directory() . '/includes/theme-filter.php');
		require_once (get_template_directory() . '/includes/frontend-enqueue.php');
		require_once (get_template_directory() . '/includes/tax-meta.php');

		if (hemelios_check_vc_status() == true) {
			require_once (get_template_directory() . '/includes/vc-functions.php');
		}
	}

	hemelios_include_library();
}

if (!function_exists('hemelios_style_schemes_output')) {
	function hemelios_style_schemes_output($custom_style = '') {
		ob_start();
		echo ('<style>'.$custom_style.'</style>');
		$content = ob_get_clean();

		return $content;
	}


}

if (!function_exists('hemelios_hex2rgba')) {
	function hemelios_hex2rgba($hex, $alpha = '') {
		$hex = str_replace("#", "", $hex);
		if (strlen($hex) == 3) {
			$r = hexdec(substr($hex, 0, 1).substr($hex, 0, 1));
			$g = hexdec(substr($hex, 1, 1).substr($hex, 1, 1));
			$b = hexdec(substr($hex, 2, 1).substr($hex, 2, 1));
		} else {
			$r = hexdec(substr($hex, 0, 2));
			$g = hexdec(substr($hex, 2, 2));
			$b = hexdec(substr($hex, 4, 2));
		}
		$rgb = $r.','.$g.','.$b;

		if ('' == $alpha) {
			return 'rgb('.$rgb.')';
		} else {
			$alpha = floatval($alpha);

			return 'rgba('.$rgb.','.$alpha.')';
		}
	}
}



function RandomString($length) {

	$key = null;

	$keys = array_merge(range(0,9), range('a', 'z'));

	for($i=0; $i < $length; $i++) {

		$key .= $keys[array_rand($keys)];

	}

	return $key;

}
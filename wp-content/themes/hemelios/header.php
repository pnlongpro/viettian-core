<!DOCTYPE html>
<!-- Open Html -->
<html <?php language_attributes(); ?>>
<!-- Open Head -->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<?php wp_head(); ?>
</head>
<!-- Close Head -->
<body <?php body_class(); ?>>
<?php
/**
 * @hooked  - hemelios_site_loading - 5
 **/
do_action( 'hemelios_before_page_wrapper' );
?>
<!-- Open Wrapper -->
<div id="wrapper">

	<?php
	/**
	 * @hooked - hemelios_page_above_header - 10
	 * @hooked - hemelios_page_top_bar - 15
	 * @hooked - hemelios_page_header - 20
	 **/
	do_action( 'hemelios_before_page_wrapper_content' );
	?>

	<!-- Open Wrapper Content -->
	<div id="wrapper-content" class="clearfix">

<?php
/**
 **/
do_action( 'hemelios_main_wrapper_content_start' );
?>
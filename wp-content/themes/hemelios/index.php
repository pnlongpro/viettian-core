<?php
if ( isset( $_REQUEST['custom-page'] ) && !empty( $_REQUEST['custom-page'] ) ) {
	do_action( 'custom-page/' . $_REQUEST['custom-page'] );
} else {
	get_header();
	hemelios_get_template( 'archive' );
	get_footer();
}


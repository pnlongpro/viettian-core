<?php
/**
 * @hooked - hemelios_page_above_header - 10
 * @hooked - hemelios_page_top_bar - 15
 * @hooked - hemelios_page_header - 20
 **/
remove_action( 'hemelios_before_page_wrapper_content', 'hemelios_page_top_bar', 15 );

$header_bg             = 'rgba(0,0,0,0)';
$header_border_color   = 'rgba(0,0,0,0)';
$header_sticky_bg      = '#222';
$header_margin_top     = '0';
$menu_background_color = 'rgba(0,0,0,0)';
$menu_text_color       = '#fff';
$hemelios_header_layout  = 'header-1';

$custom_404_style = hemelios_404_custom_style();

if ( isset( $header_border_color ) && $header_border_color != '' ) {
	$custom_404_style[] = 'header.main-header{border-bottom: 1px solid ' . $header_border_color . ' !important}';
}

if ( $header_bg != '' ) {
	$custom_404_style[] = 'body.header-overlay header.main-header{background-color: ' . $header_bg . ' !important}';
	$custom_404_style[] = 'header.header-2 .header-menu{background-color: ' . $header_bg . ' !important}';
	$custom_404_style[] = 'header.main-header{background-color: ' . $header_bg . ' !important}';
}


// Header Sticky Background Color
if ( $header_sticky_bg != '' ) {
	$custom_404_style[] = '@media screen and (min-width: 992px){ .sticky-wrapper.is-sticky header.main-header { background-color: ' . $header_sticky_bg . ' !important;} .sticky-wrapper.is-sticky header.main-header .menu-wrapper  { background-color: ' . $header_sticky_bg . ' !important;}}';

	$custom_404_style[] = '@media screen and (min-width: 992px){ header.header-2 .sticky-wrapper.is-sticky .header-2-menu-wrapper { background-color: ' . $header_sticky_bg . ' !important; }  header.header-2 .sticky-wrapper.is-sticky .header-2-menu-wrapper .menu-wrapper { background-color: ' . $header_sticky_bg . ' !important; }}';

	$custom_404_style[] = '@media screen and (min-width: 992px){ header.header-3 .sticky-wrapper.is-sticky .header-3-menu-wrapper { background-color: ' . $header_sticky_bg . ' !important; } header.header-3 .sticky-wrapper.is-sticky .header-3-menu-wrapper .menu-wrapper {  background-color: ' . $header_sticky_bg . ' !important; }}';
}


if ( isset( $header_margin_top ) && $header_margin_top != '' ) {
	$custom_404_style[] = 'header.main-header{margin-top: ' . $header_margin_top . 'px !important}';
}
if ( isset( $menu_background_color ) && $menu_background_color != '' ) {
	$custom_404_style[] = 'header.main-header .menu-wrapper{background-color: ' . $menu_background_color . ' !important}';
	$custom_404_style[] = 'header.header-2 .header-menu{background-color: ' . $menu_background_color . ' !important}';
	$custom_404_style[] = 'header.header-3 .header-3-menu-wrapper{background-color: ' . $menu_background_color . ' !important}';
}
if ( isset( $menu_text_color ) && $menu_text_color != '' ) {
	$custom_404_style[] = 'header.main-header .menu-wrapper .x-nav-menu > li.x-menu-item > a.x-menu-a-text > span {color: ' . $menu_text_color . ';}';
	$custom_404_style[] = 'header.main-header .menu-wrapper .x-nav-menu > li.x-menu-item > a.x-menu-a-text > b.x-caret:before {color: ' . $menu_text_color . ';}';
	$custom_404_style[] = '#primary-menu .header-customize .shopping-cart-wrapper .widget_shopping_cart_icon_wrapper p.cart-subtotal { color: ' . $menu_text_color . ' !important;}';
	$custom_404_style[] = '#primary-menu .header-customize .shopping-cart-wrapper .widget_shopping_cart_icon_wrapper .widget_shopping_cart_icon > i.icon { color: ' . $menu_text_color . ' !important;}';
}


get_header();
$hemelios_options  = hemelios_option();
$page_404_bg     = $hemelios_options['page_404_bg_image'];
$page_404_bg_url = '';
if ( isset( $page_404_bg ) && array_key_exists( 'url', $page_404_bg ) ) {
	$page_404_bg_url = $page_404_bg['url'];
}
?>
<section class="page-title-wrap page-title-wrap-bg" style="height: 251px; background-image: url(<?php echo esc_url( $page_404_bg_url ) ?>);">
	<div class="page-title-overlay"></div>
</section>
<div class="page404">
	<div class="container content-404">
		<?php $img_404 = get_template_directory_uri() . '/assets/images/404-img.png'; ?>
		<img src="<?php echo esc_attr( $img_404 ) ?>" alt="<?php esc_attr__( 'The page you’re looking for not found', 'hemelios' ); ?> " />

		<h2><?php echo esc_html__( '404 OOPS!', 'hemelios' ) ?> </h2>

		<div class="description"><?php echo wp_kses_post( $hemelios_options['subtitle_404'] ); ?></div>
		<div class="button">
			<a class="os-button size-lg style1" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo esc_html__( 'BACK TO HOMEPAGE', 'hemelios' ); ?> </a>
		</div>
	</div>

</div>
<?php
get_footer();
?>


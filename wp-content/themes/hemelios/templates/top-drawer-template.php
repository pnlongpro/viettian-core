<?php
$hemelios_options = hemelios_option();
$top_drawer_sidebar        = $hemelios_options['top_drawer_sidebar'];
$top_drawer_type           = $hemelios_options['top_drawer_type'];
$top_drawer_wrapper_layout = $hemelios_options['top_drawer_wrapper_layout'];
$top_drawer_class          = 'top-drawer-show';
if ( $top_drawer_type != 'show' ) {
	$top_drawer_class = 'top-drawer-hide';
}
?>
<?php if ( ( is_active_sidebar( $top_drawer_sidebar ) ) && $top_drawer_type != 'none' ): ?>
	<div id="top-drawer-area" class="hidden-sm hidden-xs">
		<div id="top-drawer-bar" class="<?php echo esc_attr( $top_drawer_class ); ?>">
			<?php if ( $top_drawer_wrapper_layout != 'full' ): ?>
			<div class="<?php echo esc_attr( $top_drawer_wrapper_layout ) ?>">
				<?php endif; ?>
				<div class="sidebar sidebar-top-drawer row">
					<?php dynamic_sidebar( $top_drawer_sidebar ); ?>
				</div>
				<?php if ( $top_drawer_wrapper_layout != 'full' ): ?>
			</div>
		<?php endif; ?>
		</div>
		<?php if ( $top_drawer_type != 'show' ): ?>
			<a href="#" class="top-drawer-toggle"></a>
		<?php endif; ?>
	</div>
<?php endif; ?>
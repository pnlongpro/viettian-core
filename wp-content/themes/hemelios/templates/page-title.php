<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/2/2015
 * Time: 2:54 PM
 */
$hemelios_options = hemelios_option();

global $post;
$prefix          = 'hemelios_';
$show_page_title = hemelios_get_post_meta_box_option( $prefix . 'show_page_title' );
if ( ( $show_page_title == - 1 ) || ( $show_page_title === '' ) ) {

	$show_page_title = $hemelios_options['show_page_title'];

	if ( is_singular( 'product' ) ) {
		$show_page_title = $hemelios_options['show_single_product_title'];
	} else {
		if ( is_singular( 'portfolio' ) ) {
			$show_page_title = $hemelios_options['show_portfolio_title'];
		} else {
			if ( is_singular( 'services' ) ) {
				$show_page_title = $hemelios_options['show_service_title'];
			} else {
				if ( is_singular( 'ourteam' ) ) {
					$show_page_title = $hemelios_options['show_our_team_title'];
				} else {
					if ( is_singular( 'post' ) ) {
						$show_page_title = $hemelios_options['show_single_blog_title'];
					}
				}
			}
		}
	}

}

if ( $show_page_title == 0 ) {
	return;
}

$page_title = hemelios_get_post_meta_box_option( $prefix . 'page_title_custom', 'type=text', get_the_ID() );
$title_default         = $hemelios_options['archive_title_default'];

if ( $page_title === '' ) {
	if ( is_singular( 'post' ) ) {
		if( $title_default != '' ){
			$page_title = $title_default;
		}elseif( $page_title != '' ){
			$page_title = rwmb_meta( $prefix . 'page_title_custom', 'type=text', get_the_ID() );
		}else{
			$page_title = esc_html__( "News", 'cortana' );
		}
	} elseif ( is_singular( 'services' ) ) {
		$page_title = 'Services';
	} else {
		$page_title = get_the_title();
	}
}

$page_description = hemelios_get_post_meta_box_option( $prefix . 'page_description', 'type=text', get_the_ID() );
//if ($page_description === '') {
//	$page_title = get_the_title();
//}

$page_title_text_color = hemelios_get_post_meta_box_option( $prefix . 'page_title_text_color', 'type=color', get_the_ID() );

$page_title_bg_color = hemelios_get_post_meta_box_option( $prefix . 'page_title_bg_color', 'type=color', get_the_ID() );

$page_title_bg_images = hemelios_get_post_meta_box_option( $prefix . 'page_title_bg_image', 'type=image&size=full', get_the_ID() );
if ( !$page_title_bg_images ) {
	$page_title_bg_image = $hemelios_options['page_title_bg_image'];

	if ( is_singular( 'product' ) ) {
		$page_title_bg_image = $hemelios_options['single_product_title_bg_image'];
	} else {
		if ( is_singular( 'portfolio' ) ) {
			$page_title_bg_image = $hemelios_options['portfolio_title_bg_image'];
		} else {
			if ( is_singular( 'services' ) ) {
				$page_title_bg_image = $hemelios_options['service_title_bg_image'];
			} else {
				if ( is_singular( 'ourteam' ) ) {
					$page_title_bg_image = $hemelios_options['our_team_title_bg_image'];
				} else {
					if ( is_singular( 'post' ) ) {
						$page_title_bg_image = $hemelios_options['single_blog_title_bg_image'];
					}
				}
			}
		}
	}

} else {
	$page_title_bg_image_id = hemelios_get_post_meta( get_the_ID(), $prefix . 'page_title_bg_image', true );
	$page_title_bg_image    = $page_title_bg_images[$page_title_bg_image_id];
}
$page_title_bg_image_url = '';
if ( isset( $page_title_bg_image ) && isset( $page_title_bg_image['url'] ) ) {
	$page_title_bg_image_url = $page_title_bg_image['url'];
}

$page_title_overlay_color      = hemelios_get_post_meta_box_option( $prefix . 'page_title_overlay_color', 'type=color', get_the_ID() );
$page_title_overlay_opacity    = '';
$enable_custom_overlay_opacity = hemelios_get_post_meta_box_option( $prefix . 'enable_custom_overlay_opacity', 'type=checkbox', get_the_ID() );
if ( $enable_custom_overlay_opacity == 1 ) {
	$page_title_overlay_opacity = hemelios_get_post_meta_box_option( $prefix . 'page_title_overlay_opacity', 'type=slider', get_the_ID() ) / 100;
}

$page_title_height = hemelios_get_post_meta_box_option( $prefix . 'page_title_height', 'type=number', get_the_ID() );
if ( $page_title_height === '' ) {

	$page_title_height = isset($hemelios_options['page_title_height']['height']) ? $hemelios_options['page_title_height']['height'] : '';

	if ( is_singular( 'product' ) ) {
		$page_title_height = isset( $hemelios_options['single_product_title_height']['height'] ) ? $hemelios_options['single_product_title_height']['height'] : '';
	} else {
		if ( is_singular( 'portfolio' ) ) {
			$page_title_height = isset( $hemelios_options['portfolio_title_height']['height'] ) ? $hemelios_options['portfolio_title_height']['height'] : '';
		} else {
			if ( is_singular( 'services' ) ) {
				$page_title_height = isset($hemelios_options['service_title_height']['height']) ? $hemelios_options['service_title_height']['height'] : '';
			} else {
				if ( is_singular( 'ourteam' ) ) {
					$page_title_height = isset($hemelios_options['our_team_title_height']['height'])?$hemelios_options['our_team_title_height']['height']:'';
				} else {
					if ( is_singular( 'post' ) ) {
						$page_title_height = isset($hemelios_options['single_blog_title_height']['height'])?$hemelios_options['single_blog_title_height']['height'] : '';
					}
				}
			}
		}
	}

}

$breadcrumbs_in_page_title = hemelios_get_post_meta_box_option( $prefix . 'breadcrumbs_in_page_title' );
if ( ( $breadcrumbs_in_page_title == - 1 ) || ( $breadcrumbs_in_page_title === '' ) ) {

	$breadcrumbs_in_page_title = $hemelios_options['breadcrumbs_in_page_title'];

	if ( is_singular( 'product' ) ) {
		$breadcrumbs_in_page_title = $hemelios_options['breadcrumbs_in_single_product_title'];
	} else {
		if ( is_singular( 'portfolio' ) ) {
			$breadcrumbs_in_page_title = $hemelios_options['breadcrumbs_in_portfolio_title'];
		} else {
			if ( is_singular( 'services' ) ) {
				$breadcrumbs_in_page_title = $hemelios_options['breadcrumbs_in_service_title'];
			} else {
				if ( is_singular( 'ourteam' ) ) {
					$breadcrumbs_in_page_title = $hemelios_options['breadcrumbs_in_our_team_title'];
				} else {
					if ( is_singular( 'post' ) ) {
						$breadcrumbs_in_page_title = $hemelios_options['breadcrumbs_in_single_blog_title'];
					}
				}
			}
		}
	}
}

$class   = array();
$class[] = 'page-title-wrap';

$custom_styles = array();

if ( $page_title_bg_image_url != '' ) {
	$class[]         = 'page-title-wrap-bg';
	$custom_styles[] = 'background-image: url(' . $page_title_bg_image_url . ');';
}

if ( $page_title_bg_color != '' ) {
	$custom_styles[] = 'background-color:' . $page_title_bg_color;
}

$custom_text_color_styles = array();
if ( $page_title_text_color != '' ) {
	$custom_text_color_styles[] = 'color:' . $page_title_text_color;
}

if ( ( $page_title_height != '' ) && ( $page_title_height > 0 ) ) {
	$custom_styles[] = 'height:' . $page_title_height;
}

$page_title_remove_margin_bottom = hemelios_get_post_meta_box_option( $prefix . 'page_title_remove_margin_bottom', 'type=checkbox', get_the_ID() );
if ( $page_title_remove_margin_bottom ) {
	$class[] = 'page-title-no-margin-bottom';
}

$class_name = join( ' ', $class );

$custom_style = '';
if ( $custom_styles ) {
	$custom_style = 'style="' . join( ';', $custom_styles ) . '"';
}

/*if (!empty($page_title_bg_image_url)) {
$custom_style.= ' data-stellar-background-ratio="0.5"';
}*/

$custom_overlay_styles = array();
if ( $page_title_overlay_color != '' ) {
	$custom_overlay_styles[] = 'background-color:' . $page_title_overlay_color;
}

if ( $page_title_overlay_opacity != '' ) {
	$custom_overlay_styles[] = 'opacity:' . $page_title_overlay_opacity;
}
$custom_overlay_style = '';
if ( $custom_overlay_styles ) {
	$custom_overlay_style = 'style="' . join( ';', $custom_overlay_styles ) . '"';
}

$custom_text_color_style = '';
if ( $custom_text_color_styles ) {
	$custom_text_color_style = 'style="' . join( ';', $custom_text_color_styles ) . '"';
}

?>
<section class="<?php echo esc_attr( $class_name ) ?>" <?php echo wp_kses_post( $custom_style ); ?>>
	<div class="page-title-overlay" <?php echo wp_kses_post( $custom_overlay_style ); ?>></div>
	<div class="container">
		<div class="page-title-inner block-center">
			<div class="block-center-inner">
				<h1 <?php echo wp_kses_post( $custom_text_color_style );
				?>><?php echo esc_html( $page_title );
					?></h1>
				<?php if ( $page_description != '' ): ?>
					<p <?php echo wp_kses_post( $custom_text_color_style ); ?>><?php echo esc_html( $page_description ) ?></p>
				<?php endif; ?>
				<?php if ( $breadcrumbs_in_page_title == 1 ) {
					hemelios_the_breadcrumb();
				} ?>
			</div>
		</div>
	</div>
</section>
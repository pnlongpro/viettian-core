<?php
/**
 **/
do_action( 'hemelios_main_wrapper_content_end' );
?>

</div>
<!-- Close Wrapper Content -->

<?php
$hemelios_options = hemelios_option();
$main_footer_class = array( 'main-footer-wrapper' );
if ( isset( $hemelios_options['footer_parallax'] ) && $hemelios_options['footer_parallax'] == '1' ) {
	$main_footer_class[] = 'enable-parallax';
}

if ( $hemelios_options['collapse_footer'] == '1' ) {
	$main_footer_class[] = 'footer-collapse-able';
}


// SHOW FOOTER
$prefix           = 'hemelios_';
$footer_show_hide = hemelios_get_post_meta_box_option( $prefix . 'footer_show_hide' );
if ( ( $footer_show_hide === '' ) ) {
	$footer_show_hide = '1';
}


?>
<?php if ( $footer_show_hide == '1' ): ?>
	<footer class="<?php echo join( ' ', $main_footer_class ) ?>">
		<div id="wrapper-footer">
			<?php
			/**
			 * @hooked - hemelios_footer_widgets - 10
			 * @hooked - hemelios_bottom_bar_filter - 20
			 *
			 **/
			do_action( 'hemelios_main_wrapper_footer' );
			?>
		</div>
	</footer>
<?php endif; ?>
</div>
<!-- Close Wrapper -->

<?php
/**
 * @hooked - hemelios_back_to_top - 5
 **/
do_action( 'hemelios_after_page_wrapper' );
?>
<?php wp_footer(); ?>
</body>
</html> <!-- end of site. what a ride! -->